

rm -r processed
mkdir processed
touch processed/summary.txt

echo 'patient count' >> processed/summary.summary.txt

cat events.csv | grep '^4[3-5][0-9]' >> processed/month.csv
cat processed/month.csv | grep -c 'ADMISS' >> processed/summary.txt
cat processed/month.csv | grep 'ADMISS' >> processed/patient_admissions.csv
cat processed/month.csv | grep 'DISCHA' >> processed/patient_discharges.csv

sed -n 's/^.*,\(Patient [0-9]*\),/\1/p' processed/patient_admissions.csv > processed/patients.txt

echo 'processing patients'
touch processed/patients.csv
echo "id,admit,discharge,los,visitcount,importer,colonized,infected" >> processed/patients.csv
cat processed/patients.txt | sed -n 's/Patient //p' | while read line
do
	touch processed/Patient_$line.txt
	cat processed/month.csv | grep "Patient $line"  >> processed/Patient_$line.txt
	ident="$(head -n1 processed/Patient_$line.txt | cut -d, -f5 | sed -n 's/Patient \([0-9]*\)/\1/p')"
	admit="$(cat processed/Patient_$line.txt | grep 'ADMISS'| cut -d, -f1)" 
	disch="$(cat processed/Patient_$line.txt | grep 'DISCHAR'| cut -d, -f1)"
	los=$(echo "scale=10; $disch-$admit" | bc )
	visits="$(cat processed/Patient_$line.txt | grep -c 'VISIT')"

	import="$(cat processed/Patient_$line.txt | grep -c 'IMPORTATION')"
	colon="$(cat processed/Patient_$line.txt | grep -c 'COLON')"
	progress="$(cat processed/Patient_$line.txt | grep -c 'PROGRESSION')"
	echo "$ident,$admit,$disch,$los,$visits,$import,$colon,$progress" >> processed/patients.csv
	thing='thing'
done 
echo 'processing nurses'
sed -n 's/^.*,\(NURSE [0-9]*\),.*/\1/p' processed/month.csv | sort | uniq >> processed/nurses.txt
touch processed/nurses.csv
echo "id,visits,handwashes,touches,hhadhere,visitperday" >> processed/nurses.csv
cat processed/nurses.txt | sed -n 's/NURSE //p' | while read line
do
	touch processed/NURSE_${line}.csv
	cat processed/month.csv | grep "NURSE $line" >> processed/NURSE_$line.csv
	visits="$(cat processed/NURSE_$line.csv | grep -c 'VISIT')"
	handwa="$(cat processed/NURSE_$line.csv | grep -c 'HANDWASH')"
	touchs="$(cat processed/NURSE_$line.csv | grep -c 'TOUCH')"
	adhere="$(echo "scale=6; $handwa/ $visits*2" | bc -q)"
	vpd___="$(echo "scale=4; $visits/40" | bc -q)"
	echo "$line,$visits,$handwa,$touchs,$adhere,$vpd___" >> processed/nurses.csv
done


echo 'processing floaters'
sed -n 's/^.*,\(NURSE_FLOAT [0-9]*\),.*/\1/p' processed/month.csv | sort | uniq >> processed/floaters.txt

cat processed/floaters.txt | sed -n 's/NURSE_FLOAT //p' | while read line
do
	touch processed/FLOAT_${line}.csv
	cat processed/month.csv | grep "FLOAT $line"  >> processed/FLOAT_$line.csv
done
