package actionevents

import java.awt.event.ActionEvent

import agents.HealthCareWorker
import granular.behavior.HandHygieneManager

class WashHandsEvent extends ActionEvent {
	
	HealthCareWorker hcw
	HandHygieneManager.Types hhType
	
	public WashHandsEvent(HealthCareWorker hcw, HandHygieneManager.Types type) {
		super(hcw, 17, "hand hygiene")
		this.hcw = hcw
		this.hhType = type
	}
}
