package actionevents

import java.awt.event.ActionEvent

import agents.HealthCareWorker;
import structure.Room

class RoomLeave extends ActionEvent {
    HealthCareWorker healthCareWorker
    Room room


    RoomLeave(HealthCareWorker hcw, Room room, String message){
	super(hcw, 7, message)
	healthCareWorker = hcw
	room = room
	assert room != null
    }

    Room getRoom() {
	return room
    }
}
