package actionevents

import java.awt.event.ActionEvent

class ContaminationTransferEvent extends ActionEvent {
	public String toucher, touchee
	double toToucher
	
	
	public ContaminationTransferEvent(String toucher, String touchee, Double toToucher) {
		super((Object)toucher, 15, "contamiationTrnsfer")
		this.toucher = toucher
		this.touchee = touchee
		this.toToucher = toToucher
	}
	
	
	public String toString() {
		return toucher + " touches " + touchee + ". " + toToucher +" transfered to toucher"
	}
	
}
