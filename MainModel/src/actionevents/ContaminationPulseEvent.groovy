package actionevents

import java.awt.event.ActionEvent

import agents.Patient
import structure.Surface

class ContaminationPulseEvent extends ActionEvent{
	public patient
	
	
	public ContaminationPulseEvent(Patient p) {
		super((Object)p, 13, "contaminationpulse")
		this.patient = p
	}

	public Patient getPatient() {
		return patient
	}	
	
	

	
}
