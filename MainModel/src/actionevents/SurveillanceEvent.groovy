package actionevents;




import java.awt.event.ActionEvent

import agents.Patient

class SurveillanceEvent extends ActionEvent {

    Patient p
    String result

    public SurveillanceEvent(Patient p, String result) {
	super((Object)p, 555, "SURVEILLANCE_RESULT")
	this.p = p
	this.result = result
    }
    @Override
    public String toString() {
	return  p.toString() + ","+ result ;
    }
}
