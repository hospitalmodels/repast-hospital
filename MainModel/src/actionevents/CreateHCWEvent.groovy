package actionevents

import java.awt.event.ActionEvent

import agents.HealthCareWorker

class CreateHCWEvent extends ActionEvent{
    public HealthCareWorker hcw

    public CreateHCWEvent(HealthCareWorker h) {
	super((Object)h, 100, "CREATE")
	hcw = h
	// TODO Auto-generated constructor stub
    }
}
