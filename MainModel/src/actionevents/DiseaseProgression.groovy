package actionevents

import java.awt.event.ActionEvent
import agents.Patient

class DiseaseProgression extends ActionEvent {
	
	public Patient patient
	
	public DiseaseProgression(Patient p) {
		super(p, 12, "disease progression")
		patient = p
	}
	
	public Patient getPatient() {
		return patient
	}
	
}
