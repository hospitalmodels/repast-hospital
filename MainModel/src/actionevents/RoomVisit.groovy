package actionevents

import java.awt.event.ActionEvent

import agents.HealthCareWorker
import agents.Patient
import granular.behavior.Vtype
import agents.Agent
import structure.Room

class RoomVisit extends ActionEvent {
	HealthCareWorker hcw
	Room room
	Patient patient
	Vtype visitType
	
	RoomVisit(Agent hcw, Room room, Patient p, Vtype type, String message){
		super(hcw,4,message)
		this.hcw = (HealthCareWorker)hcw
		this.room=room
		this.patient=p	
		this.visitType = type
	}
}
