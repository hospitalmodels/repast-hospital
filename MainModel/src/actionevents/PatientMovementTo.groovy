package actionevents

import agents.Patient
import java.awt.event.ActionEvent;
import structure.Bed
import structure.Room;

class PatientMovementTo extends ActionEvent{
	Patient patient
	Room room
	
	PatientMovementTo(Patient p, Room r){
		super(p,6,"Patient Movement To " + r.getRoomNumber())
		patient = p
		room = r 
	}
	
	
}
