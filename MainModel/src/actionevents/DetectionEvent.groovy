package actionevents

import java.awt.event.ActionEvent

import agents.Patient

class DetectionEvent extends ActionEvent {
	Patient patient
	
	
	
	public DetectionEvent(Patient p) {
		super((Object)p, 18, "detection")
		this.patient = p
	}
	
	
	public Patient getPatient() {
		return patient
	}
}
