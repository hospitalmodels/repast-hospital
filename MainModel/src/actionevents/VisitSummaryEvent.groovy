package actionevents

import java.awt.event.ActionEvent

import agents.HealthCareWorker
import agents.Patient
import granular.behavior.Vtype

class VisitSummaryEvent extends ActionEvent {
    public static int nextId = 0
    int visitId
    HealthCareWorker hcw
    Patient patient
    Vtype.TYPE visitType
    double start
    double end
    boolean isRandom
    double initHcwLoad, termHcwLoad
    double initPatientLoad,termPatientLoad
    double initNearLoad, termNearLoad
    double initFarLoad, termFarLoad
    double hcwLoadBeforeDoffAndHH
    boolean ppe
    boolean startHH, startHHsoap
    boolean endHH, endHHSoap
    int nearTouches
    int farTouches
    int patientTouches
    int independent_hh
    boolean isolation



    VisitSummaryEvent(HealthCareWorker hcw, Patient p, double now){
	super(hcw, 100, "visitsummary")
	this.visitId = nextId++
	this.hcw = hcw
	this.patient = p
	this.start = now
	this.isolation=p.hasProperty("isolated") && p.getProperty("isolated")
	if (this.isolation) {
	    println("isolated")
	}
    }

    public double getDuration() {
	return end-start
    }

    @Override
    public String toString() {
	StringBuilder sb = new StringBuilder()
	sb.append(visitId + ",")
	sb.append(hcw.toString() + ",")
	sb.append(patient.toString() + ",")
	sb.append(visitType.toString() + ",")
	sb.append(start + ",")
	sb.append(end + ",")
	sb.append((isRandom ? "1" : "0") + ",")

	sb.append(initHcwLoad + ",")
	sb.append(initPatientLoad + ",")
	sb.append((startHH?"1":"0") + ",")
	sb.append(hcwLoadBeforeDoffAndHH + ",")
	sb.append((endHH?"1":"0") + ",")
	sb.append((ppe?"1":"0") + ",")
	sb.append(termHcwLoad + ",")
	sb.append(termPatientLoad +",")
	sb.append(nearTouches + ",")
	sb.append(farTouches + ",")
	sb.append(patientTouches + ",")
	sb.append(independent_hh + ",")
	sb.append(isolation?"1":"0" )
	return sb.toString()
    }
}
