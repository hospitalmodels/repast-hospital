package actionevents

import java.awt.event.ActionEvent
import agents.Patient

class ImportationEvent extends ActionEvent {
	
		Patient patient
		
		def ImportationEvent(Patient p) {
			super((Object)p, 11, "importation event")
			this.patient = p
		}
	
}
