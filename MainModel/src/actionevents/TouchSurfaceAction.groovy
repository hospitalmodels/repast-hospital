package actionevents

import java.awt.event.ActionEvent

import agents.HealthCareWorker
import structure.Surface

class TouchSurfaceAction extends ActionEvent{

    Surface toucher,touchee
    double toucher_load, touchee_load, toucher_area, touchee_area, touchee_load_after, toucher_load_after
    HealthCareWorker hcw
    String toucheeName

    public TouchSurfaceAction(HealthCareWorker hcw, Surface toucher, Surface touchee, String toucheeName) {
	super(toucher, 10, "touchsurface")
	this.toucher = toucher
	this.touchee = touchee
	this.toucher_load = toucher.getLoad()
	this.touchee_load = touchee.getLoad()
	this.toucher_area = toucher.surfaceArea
	this.touchee_area = touchee.surfaceArea
	this.hcw = hcw
	this.toucheeName = toucheeName
    }

    public setAfterLoads(Surface toucher, Surface touchee) {
	this.touchee_load_after = touchee.getLoad()
	this.toucher_load_after = toucher.getLoad()
    }

    public String toString() {
	StringBuilder sb = new StringBuilder()
	sb.append(touchee.toString() + ",")
	sb.append(toucher_load + ",")
	sb.append(toucher_area + ",")
	sb.append(touchee_load + ",")
	sb.append(touchee_area + ",")
	sb.append(toucher_load_after + ",")
	sb.append(touchee_load_after + ",")
	sb.append(toucheeName)
	return (String) sb.toString()
    }
}
