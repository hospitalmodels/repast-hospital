package actionevents

import java.awt.event.ActionEvent
import structure.Room

class RoomCleaningEvent extends ActionEvent {
	Room room
	String type

	public RoomCleaningEvent(Room source, String type) {
		super((Object)source, 5, "Room Cleaning");
		room = source;
		this.type = type
	}
	
	public RoomCleaningEvent(Room source) {
		this(source, "REGULAR")
	}

	public Room getRoom() {
		return room
	}
	
	public String getTYpe()
	{
		return type
	}	

}
