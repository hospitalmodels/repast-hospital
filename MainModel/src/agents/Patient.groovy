/**
 * Patient.groovy - represents all patients
 * @author willy
 * 
 * This class is probably over-specified, and should just be an Agent w/ type 
 * AgentType.PATIENT.  
 * 
 * TODO: dischargeEvent should be registered in the behaviors Map, rather 
 * than specified directly right here
 * 
 */

package agents

import actionevents.PatientMovementFrom
import actionevents.PatientMovementTo
import repast.simphony.engine.schedule.ISchedulableAction;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.List


import structure.Bed
import structure.Room

class Patient extends Agent{
	
	public static int NEXTID=0
	int id
	Bed bed
	Room room
	ISchedulableAction dischargeEvent
	double admitted
	double discharged
	
	public Patient(){
		super(AgentType.PATIENT)
		id = NEXTID++
	}
	
	public setBed(Bed b){
		if (b){
			fireActionPerformed(new PatientMovementTo(this, b.room))
		} else {
			fireActionPerformed(new PatientMovementFrom(this, bed.room))
		}
		bed = b
	}

	public Bed getBed() {
		return bed
	}	
	public String toString(){
		return "Patient " + id 
	}
	
	

}
