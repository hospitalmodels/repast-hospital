package agents

import structure.Bed
import structure.Room

class Nurse extends HealthCareWorker {
	static int NEXTID=0
	int id
	
	
	Nurse(){
		super(AgentType.NURSE)
		id=NEXTID++
	}
	
	String toString(){
		"Nurse " + id
	}
}
