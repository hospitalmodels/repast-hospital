/**
 * HealthCareWorker.groovy
 * @author willy
 *
 * HealthCareWorker agent represents all HCW types, differentiated by
 * their AgentType value.
 *
 * the assignedPatients collection should probably be dropped, in favor
 * of interrogating the relevant Repast network.
 *
 */

package agents

class HealthCareWorker extends Agent {
    ArrayList<Patient> assignedPatients = new ArrayList<Patient>()
    int handHygeineOpportunities
    int handHygeieneEvents
    String hcw_type_prefix

    HealthCareWorker(AgentType type){
	super(type)
	hcw_type_prefix = type.getValue() + "_"
	assignedPatients = new ArrayList<Patient>()
    }

    public double get(String key) {
	//println(hcw_type_prefix +  key)
	return inputs.get(hcw_type_prefix + key)
    }

    public int getAssignedPatientCount() {
	return assignedPatients.size();
    }



    @Override
    public String toString() {
	return type.value + " " + agentId;
    }
}
