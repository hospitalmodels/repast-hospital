/**
 * AgentType.java 
 * @author Willy
 * 
 * This registers all types of healthcare workers.  These are used as the 
 * variable in switch statements, the keys in nested HashMaps, etc.  
 * 
 * I'm probably doing something wrong by having strings as properties...
 * Doesn't pass the smell test.
 * TODO:
 * 
 */

package agents;

public enum AgentType {
	PATIENT("PATIENT"), HEALTHCAREWORKER("HCW"), NURSE("NURSE"), NURSE_FLOAT("NURSE_FLOAT"), CNA("CNA")
	, DOCTOR("DOCTOR"), PT("PT"), RT("RT"), AGENT("AGENT"), ROOM("ROOM"), PROCESS("PROCESS");
	
	private String value;
	
	private AgentType(String v) {
		this.value = v;
	}
	
	public String getValue() {
		return value;
	}
}
