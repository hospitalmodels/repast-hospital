



package agents

import java.awt.event.ActionEvent
import java.awt.event.ActionListener

import cern.jet.random.Gamma
import groovy.beans.ListenerList as ListenerList
import processes.Process
import repast.simphony.context.Context
import repast.simphony.random.RandomHelper
import util.InputData

/*
 * Agent.groovy is the base class for all agents in the system.
 * This includes prcesses.Process, which is extended by most of the
 * system-level processes. 
 * 
 * Agent holds a list of ActionListeners and a map of behaviors (which 
 * extend Process.  Novel behaviors can be added to the behaviors map and
 * referenced and stopped later.
 * 
 * Also, this is where the Expando meta-class comes into the project.
 * @author willy
 *
 */
class Agent extends Expando{
	 
	class Surveillance {

    public Surveillance() {
      super()
      // TODO Auto-generated constructor stub
    }
  }

    def static int nextAgentId = 0
	def  InputData inputs
	def int agentId
	def Map<String,Process> behaviors
    def List<ActionListener> listeners
	def AgentType type
	// wrr: this is probably too specific a property for agents.
	def inCommonArea = false
	def List<String> eventLog

	Agent(AgentType type){
		behaviors = new HashMap<String,Process>()
		eventLog = new ArrayList<ActionEvent>()
		this.type = type
		agentId = nextAgentId++
	}

	//were using for Gamma distribution (cern.jet vs. Apache Commons)
	//kept changing.
	private Gamma getGamma(double shape, double scale) {
		double alpha = shape;
		double lambda = 1.0/scale;
		return RandomHelper.createGamma(alpha, lambda);
		
	}
	
	//wrr:  Observer pattern implementation
    public void addActionListener(ActionListener listener) {
        if ( listener == null) {
            return
        }
        if ( listeners == null) {
            listeners = []
        }
        listeners.add(listener)
    }

    public void removeActionListener(ActionListener listener) {
        if ( listener == null) {
            return
        }
        if ( listeners == null) {
            listeners = []
        }
        listeners.remove(listener)
    }

    public ActionListener[] getActionListeners() {
        Object __result = []
        if ( listeners != null) {
            __result.addAll(listeners)
        }
        return (( __result ) as ActionListener[])
    }

    public void fireActionPerformed(ActionEvent param0) {
		//eventLog.add(param0.toString())
        if ( listeners != null) {
            ArrayList<ActionListener> __list = new ArrayList<ActionListener>(listeners)
            for (def listener : __list ) {
                listener.actionPerformed(param0)
            }
        }
    }

	@Override
	public String toString() {
		return "Agent [agentId=" + agentId + ", type=" + type + "]";
	}
	
	
}
