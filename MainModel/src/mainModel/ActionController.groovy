package mainModel

import actionevents.RoomLeave;
import actionevents.RoomVisit
import agents.HealthCareWorker;;

class ActionController {
	def healthCareWorkerVisitActions(RoomVisit rve){
		HealthCareWorker hcw = rve.getHcw()
		assert rve.getRoom()
		rve.getRoom().hcwVisitActions(hcw)
	}
	
	def healthCareWorkerExitActions(RoomLeave rle){
		HealthCareWorker hcw = rle.getHcw()
		assert rle.getRoom()
		rle.getRoom().hcwLeaveActions(hcw)
	}
}
