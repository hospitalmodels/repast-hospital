/// **
// * GranularBuilder.java - Main context builder implementation for the Granular Modeling Project
// * extends repast's ContextBuilder class, which makes it eligible to the the top-level model
// object
// * in Repast.
// *
// * @author Willy Ray
// * @version 0.1
// */
// package mainModel;
//
// import java.awt.event.ActionEvent;
// import java.awt.event.ActionListener;
// import java.io.BufferedWriter;
// import java.io.FileWriter;
// import java.io.IOException;
// import java.util.ArrayList;
// import java.util.HashMap;
// import java.util.Random;
//
// import actionevents.ColonizedEvent;
// import actionevents.ContaminationPulseEvent;
// import actionevents.CreateHCWEvent;
// import actionevents.DiseaseProgression;
// import actionevents.ImportationEvent;
// import actionevents.PatientAdmitted;
// import actionevents.PatientDischarged;
// import actionevents.RoomLeave;
// import actionevents.RoomVisit;
// import actionevents.SurveillanceEvent;
// import actionevents.TouchSurfaceAction;
// import actionevents.VisitSummaryEvent;
// import actionevents.WashHandsEvent;
// import agents.Agent;
// import agents.AgentType;
// import agents.HealthCareWorker;
// import agents.Patient;
// import behavior.CommonAreaBehaviorSet;
// import cern.jet.random.Gamma;
// import granular.SimpleParams;
// import granular.behavior.NurseAssignmentManager;
// import granular.behavior.Surveillance;
// import granular.behavior.VisitRandom;
// import granular.behavior.Vtype;
// import granular.behavior.Vtype.TYPE;
// import granular.disease.Disease;
// import granular.disease.DiseaseManager;
// import metacomponents.GranularMetaConfiguration;
// import processes.DoctorAssignmentManager;
// import processes.SpecialtyServices;
// import processes.roomcleaning.CleaningShiftStart;
// import repast.simphony.context.Context;
// import repast.simphony.context.space.graph.NetworkBuilder;
// import repast.simphony.dataLoader.ContextBuilder;
// import repast.simphony.engine.environment.RunEnvironment;
// import repast.simphony.engine.schedule.ISchedule;
// import repast.simphony.engine.schedule.ScheduledMethod;
// import repast.simphony.parameter.Parameters;
// import repast.simphony.random.RandomHelper;
// import repast.simphony.space.graph.Network;
// import simple_adt.Admission;
// import simple_adt.Discharge;
// import structure.Admittable;
// import structure.Bed;
// import structure.Dischargeable;
// import structure.Room;
// import structure.Surface;
// import structure.Ward;
// import util.Chooser;
// import util.InputData;
// import util.TimeUtils;
//
// public class GranularBuilder_bak
//    implements ContextBuilder<Object>, Admittable, Dischargeable, ActionListener {
//  public Parameters params;
//  private ISchedule schedule;
//  private SimpleParams localParams = new SimpleParams();
//
//  public InputData inputs;
//  /// Sep 1, 2019 - wrr: Agent-containers
//  public Ward ward;
//  private ArrayList<Room> rooms;
//
//  /// Sep 1, 2019 - wrr: Networks and agent type collections
//  // these collections are convenience only, but they add some complexity
//  // in that they have to be managed.
//  public Network<Object> assigned, commonArea, doctorAssignments;
//  public ArrayList<HealthCareWorker> nurses, cnas, doctors, pts, rts;
//  public ArrayList<Patient> patients;
//
//  /// Sep 1, 2019 - wrr: basic facility-level processes.
//  private Admission admission;
//  private Discharge discharger;
//  private CleaningShiftStart cleaning;
//  private DoctorAssignmentManager doctorAssignmentManager;
//  private NurseAssignmentManager nurseContacts;
//  public SpecialtyServices specialtyServices;
//
//  /// Sep 1, 2019 - wrr: Granular-specific processes
//  private DiseaseManager diseaseMan;
//  private Surveillance surveillance;
//
//  /// Sep 1, 2019 - wrr: Collections of distributions that govern visit type
//  // selection and visit duration selection per hcw type.
//  public HashMap<String, Gamma> visitDistributions;
//
//  public HashMap<Vtype.TYPE, Vtype> vTypes;
//
//  private ArrayList<Patient> dischargedPatients;
//
//  // private boolean outputLosRows = false;
//
//  private FileWriter eventWriter;
//  private FileWriter summaryWriter;
//  /// Aug 31, 2019 - wrr: top-level parameters
//  private int roomCount = this.localParams.getRoomCount();
//  private double nursePerRoom = 0.5;
//  private int doctorCount = 2;
//  private int cnaCount = 2;
//  private int rtCount = 2;
//  private int ptCount = 2;
//
//  /// Aug 31, 2019 - wrr: Daily actions
//  @ScheduledMethod(start = 1.0, interval = 0.5)
//  public void daily() {
//    this.cleaning.start();
//  }
//
//  public int getSurveillance() {
//    System.out.println(RunEnvironment.getInstance().getParameters().getBoolean("surveillance"));
//    if (RunEnvironment.getInstance().getParameters().getBoolean("surveillance")) {
//      return 1;
//    }
//    return 0;
//  }
//
//  @ScheduledMethod(start = 1.0, interval = 0.5)
//  public void halfDayActions() {
//    cleanAllHands();
//    shiftChange();
//  }
//
//  public void shiftChange() {
//    this.nurseContacts.newShift();
//  }
//
//  public void cleanAllHands() {
//    ArrayList<HealthCareWorker> all = new ArrayList<HealthCareWorker>();
//    all.addAll(this.cnas);
//    all.addAll(this.doctors);
//    all.addAll(this.pts);
//    all.addAll(this.rts);
//    for (HealthCareWorker hcw : all) {
//      Surface s = (Surface) hcw.getProperty("surface");
//      s.clear(this.schedule.getTickCount());
//    }
//  }
//
//  /// aug 31, 2019 - wrr: end time and other actions
//  @ScheduledMethod(start = 365, interval = 1)
//  public void enddate() throws IOException {
//    // int batch = this.params.getInteger("extra_samples");
//    // System.out.println("end - " + batch);
//    boolean output_events = this.params.getBoolean("output_event_file");
//    if (output_events) {
//      BufferedWriter writer = new BufferedWriter(new FileWriter("los.txt"));
//
//      writer.append("patient_id,admit,discharge,los\n");
//      for (Patient p : this.dischargedPatients) {
//        double los = p.getDischarged() - p.getAdmitted();
//        if (los > 0) {
//          writer.append(
//              p.getAgentId() + "," + p.getAdmitted() + "," + p.getDischarged() + "," + los +
// "\n");
//        }
//      }
//      writer.close();
//    }
//
//    RunEnvironment.getInstance().endRun();
//  }
//
//  // were using for Gamma distribution (cern.jet vs. Apache Commons)
//  // kept changing.
//  private Gamma getGamma(double shape, double scale) {
//    double alpha = shape;
//    double lambda = 1.0 / scale;
//    return RandomHelper.createGamma(alpha, lambda);
//  }
//
//  /**
//   * This function builds a model and returns the built-up context. Repast Simphony calls this
//   * function at the beginning of the model setup.
//   *
//   * @param context a Repast Context<Object> this is handled by the repast runtime.
//   * @return context The context object that represents our model
//   */
//  @Override
//  public Context<Object> build(Context<Object> context) {
//
//    /// Sep 1, 2019 - wrr: get a reference to the scheduler and schedule this class's
//    // actions
//    this.params = RunEnvironment.getInstance().getParameters();
//
//    // System.out.println(System.getProperty("user.dir"));
//    this.inputs = new InputData("/home/willy/workspace/repast-hospital/MainModel/inputs.conf");
//
//    this.vTypes = new HashMap<Vtype.TYPE, Vtype>();
//    this.vTypes.put(Vtype.TYPE.ONE, new Vtype(Vtype.TYPE.ONE, this.inputs.getData()));
//    this.vTypes.put(Vtype.TYPE.TWO, new Vtype(TYPE.TWO, this.inputs.getData()));
//    this.vTypes.put(Vtype.TYPE.THREE, new Vtype(TYPE.THREE, this.inputs.getData()));
//    this.vTypes.put(Vtype.TYPE.FOUR, new Vtype(TYPE.FOUR, this.inputs.getData()));
//    this.vTypes.put(Vtype.TYPE.FIVE, new Vtype(TYPE.FIVE, this.inputs.getData()));
//
//    this.schedule = TimeUtils.getSchedule();
//    this.schedule.schedule(this);
//
//    this.specialtyServices = new SpecialtyServices(this.inputs);
//    // Sep 1, 2019 - wrr: create wards, rooms, and hcws
//    this.ward = new Ward();
//    this.ward.setId("MainModel");
//    this.rooms = new ArrayList<Room>();
//    createRooms(this.roomCount);
//    this.nurses = createNurses(this.ward, this.nursePerRoom);
//    this.cnas = createCnas(this.ward, this.cnaCount);
//    this.doctors = createDoctors(this.ward, this.doctorCount);
//    this.pts = createPts(this.ward, this.ptCount);
//    this.rts = createRts(this.ward, this.rtCount);
//
//    // Aug 31, 2019 - wrr: this java class can't do metaprogramming on the groovy classes.
//    // so this method configures the metaclasses of this project.
//    GranularMetaConfiguration.configure();
//
//    // Sep 1, 2019 - wrr: set up networks.
//    NetworkBuilder<Object> assignedBuilder =
//        new NetworkBuilder<Object>("assigned", this.ward, false);
//    this.assigned = assignedBuilder.buildNetwork();
//    NetworkBuilder<Object> commonBuider =
//        new NetworkBuilder<Object>("commonArea", this.ward, false);
//    this.commonArea = commonBuider.buildNetwork();
//    NetworkBuilder<Object> doctorBuilder = new NetworkBuilder<Object>("doctors", this.ward,
// false);
//    this.doctorAssignments = doctorBuilder.buildNetwork();
//
//    // Sep 1, 2019 - wrr: These are actually the intra-visit times for each HCW type.
//    // TODO : refactor this so it's named more appropriately
//    this.visitDistributions = new HashMap<String, Gamma>();
//    double nurseShape = this.inputs.get("NURSE_gamma_minutes_to_next_assigned_visit_shape");
//    double nurseScale = this.inputs.get("NURSE_gamma_minutes_to_next_assigned_visit_scale");
//    this.visitDistributions.put("NURSE", getGamma(nurseShape, nurseScale));
//    double shape = this.inputs.get("NURSE_gamma_minutes_to_next_visit_shape");
//    double scale = this.inputs.get("NURSE_gamma_minutes_to_next_visit_scale");
//    this.visitDistributions.put("NURSE_RANDOM", getGamma(shape, scale));
//    shape = this.inputs.get("CNA_gamma_minutes_to_next_visit_shape");
//    scale = this.inputs.get("CNA_gamma_minutes_to_next_visit_scale");
//    this.visitDistributions.put("CNA", getGamma(shape, scale));
//    shape = this.inputs.get("DOCTOR_gamma_minutes_to_next_visit_shape");
//    scale = this.inputs.get("DOCTOR_gamma_minutes_to_next_visit_scale");
//    this.visitDistributions.put("DOCTOR", getGamma(shape, scale));
//    shape = this.inputs.get("PT_gamma_minutes_to_next_visit_shape");
//    scale = this.inputs.get("PT_gamma_minutes_to_next_visit_scale");
//    this.visitDistributions.put("PT", getGamma(shape, scale));
//    shape = this.inputs.get("RT_gamma_minutes_to_next_visit_shape");
//    scale = this.inputs.get("RT_gamma_minutes_to_next_visit_scale");
//    this.visitDistributions.put("RT", getGamma(shape, scale));
//
//    // Sep 1, 2019 - wrr: Disease manager creates and manages the individual patient's disease
//    // submodels
//    this.diseaseMan = new DiseaseManager(this);
//    this.diseaseMan.addActionListener(this);
//    // TODO: Can this be added to this instead of ward?
//    this.ward.add(this.diseaseMan);
//
//    // Sep 1, 2019 - wrr: start admissions and discharger.  Admission(1, ...) is "one per day".
//    this.admission = new Admission(.15, this);
//    this.admission.start();
//    this.discharger = new Discharge(5.5, this);
//    this.dischargedPatients = new ArrayList<Patient>();
//
//    this.patients = new ArrayList<Patient>();
//
//    // Sep 1, 2019 - wrr: these manage the nurse and doctor specific-patient-assignments.
//    this.nurseContacts = new NurseAssignmentManager(this);
//    this.doctorAssignmentManager =
//        new DoctorAssignmentManager(this.doctors, this.ward, this, this.doctorAssignments);
//
//    // wrr: Set Surveillace
//    this.surveillance = new Surveillance(0.0);
//    this.surveillance.stop();
//    this.surveillance.addActionListener(this);
//
//    // Sep 1, 2019 - wrr: HCWs have been created.  These loops configure them. Add them to the
//    // create functions?
//    // TODO: evaulate whether these should be added to the create functions
//    for (Object n : this.nurses) {
//      if (n instanceof HealthCareWorker && ((HealthCareWorker) n).getType() == AgentType.NURSE) {
//        HealthCareWorker nurse = (HealthCareWorker) n;
//        nurse.setProperty("surface", new Surface(nurse.toString(), 23));
//        VisitRandom nrv = new VisitRandom(this.visitDistributions.get("NURSE_RANDOM"), nurse,
// this);
//        nurse.getBehaviors().put("NURSE_RANDOM", nrv);
//        ArrayList<Double> vtp = new ArrayList<Double>();
//        vtp.add(this.inputs.get("NURSE_type_one_visit_prob"));
//        vtp.add(this.inputs.get("NURSE_type_two_visit_prob"));
//        vtp.add(this.inputs.get("NURSE_type_three_visit_prob"));
//        vtp.add(this.inputs.get("NURSE_type_four_visit_prob"));
//        vtp.add(this.inputs.get("NURSE_type_five_visit_prob"));
//        nrv.setIsRandom(true);
//        nrv.addActionListener(this);
//        nrv.start();
//
//        VisitRandom nav = new VisitRandom(this.visitDistributions.get("NURSE"), nurse, this);
//        nurse.getBehaviors().put("NURSE", nav);
//        vtp = null;
//        vtp = new ArrayList<Double>();
//        vtp.add(this.inputs.get("NURSE_type_one_assigned_visit_prob"));
//        vtp.add(this.inputs.get("NURSE_type_two_assigned_visit_prob"));
//        vtp.add(this.inputs.get("NURSE_type_three_assigned_visit_prob"));
//        vtp.add(this.inputs.get("NURSE_type_four_assigned_visit_prob"));
//        vtp.add(this.inputs.get("NURSE_type_five_assigned_visit_prob"));
//        nav.setVisitTypeProbs(vtp);
//        nav.setIsRandom(false);
//        nav.addActionListener(this);
//        nav.patientCollection = nurse.getAssignedPatients();
//        nav.start();
//      }
//    }
//
//    for (HealthCareWorker cna : this.cnas) {
//      if (cna instanceof HealthCareWorker) {
//        HealthCareWorker c = cna;
//        cna.setProperty("surface", new Surface(cna.toString(), 23));
//        VisitRandom crv = new VisitRandom(this.visitDistributions.get("CNA"), cna, this);
//        crv.getBehaviors().put("RANDOM", crv);
//        crv.addActionListener(this);
//        crv.start();
//      }
//    }
//
//    for (HealthCareWorker doctor : this.doctors) {
//      if (doctor instanceof HealthCareWorker) {
//        doctor.setProperty("surface", new Surface(doctor.toString(), 23));
//        if (doctor.getType() == AgentType.DOCTOR) {
//          VisitRandom drv = new VisitRandom(this.visitDistributions.get("DOCTOR"), doctor, this);
//          doctor.getBehaviors().put("RANDOM", drv);
//          drv.addActionListener(this);
//          drv.start();
//        }
//      }
//    }
//
//    for (HealthCareWorker pt : this.pts) {
//      if (pt instanceof HealthCareWorker) {
//        HealthCareWorker p = pt;
//        pt.setProperty("surface", new Surface(pt.toString(), 23));
//        if (pt.getType() == AgentType.PT) {
//          VisitRandom prv = new VisitRandom(this.visitDistributions.get("PT"), pt, this);
//          prv.patientCollection = this.specialtyServices.needsPT;
//          pt.getBehaviors().put("RANDOM", prv);
//          prv.addActionListener(this);
//          prv.start();
//        }
//      }
//    }
//
//    for (HealthCareWorker rt : this.rts) {
//      if (rt instanceof HealthCareWorker) {
//        HealthCareWorker r = rt;
//        rt.setProperty("surface", new Surface(rt.toString(), 23));
//        if (rt.getType() == AgentType.RT) {
//          VisitRandom rrv = new VisitRandom(this.visitDistributions.get("RT"), rt, this);
//          rrv.patientCollection = this.specialtyServices.needsRT;
//          rt.getBehaviors().put("RANDOM", rrv);
//          rrv.addActionListener(this);
//          rrv.start();
//        }
//      }
//    }
//
//    for (HealthCareWorker hcw : getAllHealthCareWorkers()) {
//      hcw.setInputs(this.inputs);
//    }
//
//    // Sep 1, 2019 - wrr: Cleaning subsystem handles routine cleaning and terminal cleaning
//    // cleaning shift:  shift length of 1 means we
//    this.cleaning = new CleaningShiftStart(0.29166, 0.166, this.ward.getRooms(), this);
//    this.cleaning.addActionListener(this);
//    this.cleaning.addActionListener(this.diseaseMan);
//
//    return this.ward;
//  }
//
//  public ArrayList<HealthCareWorker> getAllHealthCareWorkers() {
//    ArrayList<HealthCareWorker> all = new ArrayList<HealthCareWorker>();
//    all.addAll(this.nurses);
//    all.addAll(this.cnas);
//    all.addAll(this.doctors);
//    all.addAll(this.pts);
//    all.addAll(this.rts);
//    return all;
//  }
//
//  // Process implementations
//
//  // Sep 1, 2019 - wrr: TODO:  This would be better if this wasn't called directly from
//  // the admissions submodel, but was a response to some kind of AttemptAdmission actionEvent
//  @Override
//  public void admit() {
//
//    // Sep 1, 2019 - wrr: If there are available beds...
//    if (this.ward.getVacancy() > 0) {
//
//      // Sep 1, 2019 - wrr: Get a list of empty rooms
//      ArrayList<Room> emptyRooms = new ArrayList<Room>();
//      for (Room r : this.ward.getRooms()) {
//        if (r.getOccupancy() == 0) {
//          emptyRooms.add(r);
//        }
//      }
//      // Sep 1, 2019 - wrr: If there are available beds... this is a double-check
//      if (emptyRooms.size() > 0) {
//        // Sep 1, 2019 - wrr: pick a random room from that list.
//        Room targetRoom = emptyRooms.get(new Random().nextInt(emptyRooms.size()));
//        // Sep 1, 2019 - wrr: create a patient
//        Agent a = new Patient();
//        Patient p = (Patient) a;
//
//        // TODO:  Don't forget to remove this
//        // p.setProperty("isolation", true);
//
//        // Sep 1, 2019 - wrr: TODO: roll this stuff into a "configure patient" function
//        p.setProperty("surface", new Surface(p.toString(), 10, 300));
//        p.addActionListener(this);
//        p.addActionListener(this.doctorAssignmentManager);
//        p.addActionListener(this.diseaseMan);
//
//        if (Chooser.randomTrue(this.params.getDouble("all_cause_isolation"))) {
//          p.setProperty("isolated", true);
//        }
//
//        this.discharger.newPatient(p);
//        this.specialtyServices.newPatient(p);
//
//        // Sep 1, 2019 - wrr: this is where control should return to local
//        PatientAdmitted pa = new PatientAdmitted(p);
//        p.fireActionPerformed(pa);
//        targetRoom.getBeds().get(0).setPatient(p);
//        p.setBed(targetRoom.getBeds().get(0));
//        p.setRoom(targetRoom);
//        p.setAdmitted(this.schedule.getTickCount()); // // Apr 1, 2020 - wrr: admission time
//
//        // Sep 1, 2019 - wrr: ward is a repast context.  patients is a simple ArrayList
// convenience
//        // function
//        this.ward.add(p);
//        this.patients.add(p);
//        this.nurseContacts.newPatient(p);
//
//        event(
//            "ADMISSION",
//            "add patient " + p.getId() + " to Room " + targetRoom.getRoomNumber(),
//            a,
//            null,
//            p.getId() + "," + targetRoom.getAgentId());
//
//        this.surveillance.actionPerformed(pa);
//      } else {
//        event("ADMISSION", "tried to add patient - couldn't find room", null, null, "");
//      }
//
//    } else {
//      // no action, hospital is full
//      event("ADMISSION", "Hospital is full", null, null, "");
//    }
//  }
//
//  // Sep 1, 2019 - wrr: Again TODO: this should be triggered by an actionEvent from the discharge
//  // system
//  @Override
//  public void discharge(Agent a) {
//    Patient p = (Patient) a;
//    if (p.getProperty("delayDischarge") != null
//        && (boolean) p.getProperty("delayDischarge") == true) {
//      p.setProperty("delayDischarge", false);
//      System.out.println("delay discharge " + p.toString());
//      return;
//    }
//    Room r = p.getRoom();
//
//    p.getBed().setPatient(null);
//    p.setBed(null);
//    this.specialtyServices.discharge(p);
//    p.fireActionPerformed(new PatientDischarged(p));
//    p.setDischarged(this.schedule.getTickCount());
//    if (TimeUtils.getSchedule().getTickCount() > 0) {
//      this.dischargedPatients.add(p);
//    }
//    event(
//        "DISCHARGE",
//        "Discharged patient " + p.toString(),
//        p,
//        null,
//        p.getAgentId() + "," + r.getAgentId());
//
//    // Sep 1, 2019 - wrr: Remove all hcw assignments from the patient
//    for (Object hcw : this.assigned.getSuccessors(p)) {
//      if (hcw instanceof HealthCareWorker) {
//        HealthCareWorker h = (HealthCareWorker) hcw;
//        h.getAssignedPatients().remove(p);
//      }
//    }
//
//    // Sep 1, 2019 - wrr: stop the disease processes if they exist
//    if (p.getProperty("disease") != null) {
//      ((Disease) p.getProperty("disease")).stop();
//    }
//
//    // Sep 1, 2019 - wrr: TODO: would be better if disease and cleaning picked this information up
//    // from
//    // the actionEvent fired above.
//    this.diseaseMan.discharge(p);
//    this.ward.remove(p);
//    this.patients.remove(p);
//    if (r != null) {
//      this.cleaning.terminal(r);
//    }
//  }
//
//  /**
//   * Creates the right number of rooms, creates bed, zones, touchable objects
//   *
//   * @param int room count
//   */
//  private void createRooms(int count) {
//    event("INIT", "create " + count + " rooms", null, null, "");
//    for (int i = 0; i < count; i++) {
//      Room r = new Room(i + "", true, false, false, false, false, this.ward);
//      r.setCapacity(1);
//      Bed b = new Bed(r);
//      r.addBed(b);
//      b.setRoom(r);
//      Surface touchscreen = new Surface("touchscreen", 2, 0);
//      Surface table = new Surface("table", 6, 0);
//      r.getFarZone().add(touchscreen);
//      r.getFarZone().add(table);
//      Surface cart = new Surface("bedrail", 2, 0);
//      Surface keyboard = new Surface("keyboard", 2, 0);
//      r.getNearZone().add(cart);
//      r.getNearZone().add(keyboard);
//
//      this.rooms.add(r);
//      this.ward.addRoom(r);
//    }
//  }
//
//  // Sep 1, 2019 - wrr: All of these create<HCW> functions could be rolled into one, with the
//  // agent type as an argument
//
//  private ArrayList<HealthCareWorker> createNurses(Ward ward, double ratio) {
//
//    // Sep 1, 2019 - wrr: nurse:room ratio = 1:2
//    int bedCount = ward.getRooms().size();
//    int nurseCount = (int) java.lang.Math.ceil(bedCount * ratio);
//    event("INIT", "Create " + nurseCount + " nurses", null, null, "");
//    ArrayList<HealthCareWorker> nurses = new ArrayList<HealthCareWorker>();
//    for (int i = 0; i < nurseCount; i++) {
//      HealthCareWorker n = new HealthCareWorker(AgentType.NURSE);
//      n.setInCommonArea(true);
//      ward.add(n);
//      actionPerformed(new CreateHCWEvent(n));
//
//      // Sep 1, 2019 - wrr: sucks to be this dumb, right?
//      ward.getNurses().add(n);
//      nurses.add(n);
//
//      // Sep 1, 2019 - wrr: TODO: this should be in a configure nurses function?
//      CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, this.commonArea);
//      n.getBehaviors().put("COMMONMIX", c_mix);
//      c_mix.start();
//      n.setProperty("surface", new Surface(n.toString(), this.schedule.getTickCount()));
//    }
//    return nurses;
//  }
//
//  private ArrayList<HealthCareWorker> createCnas(Ward ward, int cnaCount) {
//    ArrayList<HealthCareWorker> cnas = new ArrayList<HealthCareWorker>();
//    for (int i = 0; i < cnaCount; i++) {
//      HealthCareWorker n = new HealthCareWorker(AgentType.CNA);
//      n.setInCommonArea(true);
//      ward.add(n);
//
//      cnas.add(n);
//      CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, this.commonArea);
//      n.getBehaviors().put("COMMONMIX", c_mix);
//      c_mix.start();
//      n.setProperty("surface", new Surface(n.toString(), this.schedule.getTickCount()));
//    }
//    return cnas;
//  }
//
//  private ArrayList<HealthCareWorker> createDoctors(Ward ward, int doctorCount) {
//    ArrayList<HealthCareWorker> docs = new ArrayList<HealthCareWorker>();
//    for (int i = 0; i < doctorCount; i++) {
//      HealthCareWorker n = new HealthCareWorker(AgentType.DOCTOR);
//      n.setInCommonArea(true);
//      ward.add(n);
//
//      docs.add(n);
//      CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, this.commonArea);
//      n.getBehaviors().put("COMMONMIX", c_mix);
//      c_mix.start();
//      n.setProperty("surface", new Surface(n.toString(), this.schedule.getTickCount()));
//    }
//    return docs;
//  }
//
//  private ArrayList<HealthCareWorker> createPts(Ward ward, int ptCount) {
//    ArrayList<HealthCareWorker> pts = new ArrayList<HealthCareWorker>();
//    for (int i = 0; i < ptCount; i++) {
//      HealthCareWorker n = new HealthCareWorker(AgentType.PT);
//      n.setInCommonArea(true);
//      ward.add(n);
//
//      pts.add(n);
//      CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, this.commonArea);
//      n.getBehaviors().put("COMMONMIX", c_mix);
//      c_mix.start();
//      n.setProperty("surface", new Surface(n.toString(), this.schedule.getTickCount()));
//    }
//    return pts;
//  }
//
//  private ArrayList<HealthCareWorker> createRts(Ward ward, int rtCount) {
//    ArrayList<HealthCareWorker> rts = new ArrayList<HealthCareWorker>();
//    for (int i = 0; i < rtCount; i++) {
//      HealthCareWorker n = new HealthCareWorker(AgentType.RT);
//      n.setInCommonArea(true);
//      ward.add(n);
//
//      rts.add(n);
//      CommonAreaBehaviorSet c_mix = new CommonAreaBehaviorSet(.05, n, this, this.commonArea);
//      n.getBehaviors().put("COMMONMIX", c_mix);
//      c_mix.start();
//      n.setProperty("surface", new Surface(n.toString(), this.schedule.getTickCount()));
//    }
//    return rts;
//  }
//
//  // Sep 1, 2019 - wrr: implementation of the java.awt.event.actionListener interface
//  @Override
//  public void actionPerformed(ActionEvent e) {
//    boolean output_events = this.params.getBoolean("output_event_file");
//
//    if (e instanceof SurveillanceEvent) {
//      SurveillanceEvent se = (SurveillanceEvent) e;
//      event("SURVEILLANCE", se.toString(), null, null, "");
//    }
//
//    if (e instanceof TouchSurfaceAction) {
//      TouchSurfaceAction cta = (TouchSurfaceAction) e;
//      event("TOUCH", cta.toString(), cta.getHcw(), null, "");
//      // System.out.println(cta.toString());
//    }
//
//    if (e instanceof CreateHCWEvent) {
//      CreateHCWEvent che = (CreateHCWEvent) e;
//      event("CREATE_HCW", che.hcw, che.hcw, che.hcw.toString());
//    }
//    if (e instanceof VisitSummaryEvent) {
//      processVisitSummary((VisitSummaryEvent) e);
//    }
//
//    if (e instanceof ImportationEvent) {
//      ImportationEvent ie = (ImportationEvent) e;
//      Agent a = ie.getPatient();
//      String st = "impCol";
//      Disease d = (Disease) a.getProperty("disease");
//      if (d.state == Disease.State.INFECTED) {
//        st = "impInf";
//      }
//      event("IMPORTATION", "by " + ie.getPatient().toString() + "," + st, a, null, "");
//    }
//
//    if (e instanceof ContaminationPulseEvent) {
//      ContaminationPulseEvent ce = (ContaminationPulseEvent) e;
//      Agent a = ce.getPatient();
//      Patient p = ce.getPatient();
//      event(
//          "CONTAMINATION_PULSE",
//          p.toString(),
//          a,
//          null,
//          p.getAgentId() + "," + p.getRoom().getAgentId());
//      if (TimeUtils.getSchedule().getTickCount() >= 60) {
//        ce.patient.hashCode();
//      }
//    }
//
//    if (e instanceof DiseaseProgression) {
//      DiseaseProgression de = (DiseaseProgression) e;
//      Agent a = de.getPatient();
//      event(
//          "PROGRESSION",
//          de.patient.toString(),
//          a,
//          null,
//          de.patient.getAgentId() + "," + de.patient.getRoom().getAgentId());
//    }
//
//    if (e instanceof TouchSurfaceAction) {}
//
//    if (e instanceof ColonizedEvent) {
//      ColonizedEvent ce = (ColonizedEvent) e;
//      Agent a = ce.patient;
//      event(
//          "COLONIZATION",
//          ce.patient.toString() + " (load: " + ce.load + ")",
//          a,
//          null,
//          ce.patient.getAgentId() + "," + ce.patient.getRoom().getAgentId());
//    }
//
//    if (e instanceof RoomVisit) {}
//
//    if (e instanceof RoomLeave) {}
//
//    if (e instanceof VisitSummaryEvent) {
//      VisitSummaryEvent vse = (VisitSummaryEvent) e;
//      if (this.params.getInteger("postVisitCleaning") == 1) {
//
//        if (vse.getVisitType() == TYPE.FIVE || vse.getVisitType() == TYPE.FOUR) {
//          this.cleaning.cleanRoomOnce(vse.getPatient().getRoom());
//        }
//      }
//    }
//
//    if (e instanceof WashHandsEvent) {
//      WashHandsEvent whe = (WashHandsEvent) e;
//      Agent a = whe.getHcw();
//
//      this.diseaseMan.actionPerformed(e);
//      event(
//          "HANDWASH", whe.getHcw().toString() + " with " + whe.getHhType().toString(), a, null,
// "");
//    }
//  }
//
//  public void processVisitSummary(VisitSummaryEvent vse) {
//    boolean output_events = this.params.getBoolean("output_event_file");
//    if (!output_events) {
//      return;
//    }
//    if (this.summaryWriter == null) {
//      try {
//        this.summaryWriter = new FileWriter("summaries.csv");
//        this.summaryWriter.write(
//            "visitid,hcw,patient,type,starttime"
//                + ",endtime,isRandom,inithcwload,initpatientload,"
//                + "starthh,hcwloadBeforeDoffAndHH,endHH,ppe,terminalHcwLoad,"
//                + "TerminalPatientLoad,nearTouches,farTouches,patientTouches,"
//                + "independent_hh,isolation");
//        this.summaryWriter.write('\n');
//      } catch (IOException e) {
//        System.out.println("Nope");
//        e.printStackTrace();
//      }
//    }
//
//    try {
//
//      this.summaryWriter.write(vse.toString());
//      this.summaryWriter.write('\n');
//
//    } catch (IOException e) {
//
//      e.printStackTrace();
//    }
//  }
//
//  public void event(String type, Agent actor, Agent objective, String line) {
//    event(type, line, actor, objective, "");
//  }
//
//  // Sep 1, 2019 - wrr: This prints out events to a file
//  public void event(String type, String line, Agent actor, Agent objective, String patientroom) {
//
//    boolean output_events = this.params.getBoolean("output_event_file");
//    if (output_events) {
//      String actorVal = "";
//      if (actor != null) {
//        actorVal = actor.toString();
//      }
//
//      String objectiveVal = "";
//      if (objective != null) {
//        objectiveVal = objective.toString();
//      }
//
//      if (this.schedule.getTickCount() < 0) {
//        return;
//      }
//      if (this.eventWriter == null) {
//        try {
//          this.eventWriter = new FileWriter("events.csv");
//        } catch (IOException e) {
//          System.out.println("Nope");
//          e.printStackTrace();
//        }
//      }
//
//      try {
//
//        this.eventWriter.write(
//            this.schedule.getTickCount()
//                + ","
//                + type
//                + ","
//                + line
//                + ","
//                + actorVal
//                + ","
//                + objectiveVal
//                + "\n");
//      } catch (IOException e) {
//
//        e.printStackTrace();
//      }
//    }
//  }
// }
