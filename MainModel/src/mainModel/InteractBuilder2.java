package mainModel;
// test
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import actionevents.PatientAdmitted;
import actionevents.PatientDischarged;
import actionevents.RoomLeave;
import actionevents.RoomVisit;
import agents.Agent;
import agents.Nurse;
import agents.Patient;
import behavior.RoomVisitDuration;
import behavior.StochasticRoomVisit;
import datacollectors.RuntimeData;
import metacomponents.RoomVisitComponent;
import repast.simphony.context.Context;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduledMethod;
import simple_adt.Admission;
import simple_adt.Discharge;
import structure.Admittable;
import structure.Bed;
import structure.Dischargeable;
import structure.Facility;
import structure.Room;
import structure.Ward;
import util.OutputFile;
import util.TimeUtils;

public class InteractBuilder2
    implements ContextBuilder<Object>, Admittable, Dischargeable, ActionListener {
  private Facility hospital;
  private Ward sicu, tele, acute, medsurg;
  private ArrayList<Room> rooms;
  private Admission admission;
  private Discharge discharger;
  private ISchedule schedule;
  private ActionController actionController;
  private RuntimeData data;
  private OutputFile file;

  @ScheduledMethod(start = 1, interval = 1)
  public void daily() {}

  @ScheduledMethod(start = 90, interval = 1)
  public void endDate() {
    RunEnvironment.getInstance().endRun();
  }

  @Override
  public Context<Object> build(Context<Object> context) {
    String outputHead = "tick,eventid,source,data";
    this.file = new OutputFile(outputHead, "outputs.txt");
    this.file.createFile();
    // Add specific methods to metaClasses
    RoomVisitComponent.configureRoomVisit();

    this.schedule =
        repast.simphony.engine.environment.RunEnvironment.getInstance().getCurrentSchedule();
    this.hospital = new Facility();
    this.rooms = new ArrayList<Room>();
    this.actionController = new ActionController();
    buildWards();
    buildRooms();
    buildBeds();
    calculateCapacity();
    buildNurses();
    // buildCnas();
    // buildPts();
    // buildOts();
    // buildRts();
    // buildMds();
    // buildWardMds();
    // buildPharmacists();
    this.data = new RuntimeData(this.schedule);
    this.schedule.schedule(this);

    this.admission = new Admission(.1, this);
    this.admission.start();

    // TODO:  Build transfer system
    // When ward stay is over, there's a transfer probability
    // matrix that determines flow to next ward.
    // this is is competition with the discharge timer

    this.discharger = new Discharge(5.5, this);

    // cleaning = new CleaningShiftStart(TimeUtils.HOUR*2, TimeUtils.HOUR*22, rooms);
    // cleaning.start();

    this.hospital.setId("MainModel");
    return this.hospital;
  }

  // Process implementations
  @Override
  public void admit() {
    if (this.hospital.getVacancy() > 0) {

      Bed targetBed =
          (Bed)
              this.hospital
                  .getEmptyBeds()
                  .get(new Random().nextInt(this.hospital.getEmptyBeds().size()));
      Patient p = new Patient();
      p.addActionListener(this);
      p.fireActionPerformed(new PatientAdmitted(p));
      targetBed.setPatient(p);
      p.setBed(targetBed);

      this.hospital.add(p);

    } else {
      // no action, hospital is full
    }
  }

  @Override
  public void discharge(Agent a) {
    Patient p = (Patient) a;
    p.getBed().setPatient(null);
    p.setBed(null);
    p.fireActionPerformed(new PatientDischarged(p));
    this.hospital.remove(p);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String outputLine =
        this.schedule.getTickCount()
            + ","
            + e.getID()
            + ","
            + e.getSource().toString()
            + ","
            + e.getActionCommand();
    this.file.writeLine(outputLine);
    // System.out.println(schedule.getTickCount() +   "," + e.getSource().toString() + "," +
    // e.getActionCommand());
    if (e instanceof PatientAdmitted) {
      this.discharger.newPatient(((PatientAdmitted) e).getPatient());
    }
    if (e instanceof PatientDischarged) {
      // do patient discharge things
    }
    if (e instanceof RoomVisit) {
      RoomVisit rve = (RoomVisit) e;
      rve.getHcw().getBehaviors().get("ROOMVISIT").stop();
      RoomVisitDuration visitLength =
          new RoomVisitDuration(rve.getHcw(), rve.getRoom(), 15 * TimeUtils.MINUTE);
      visitLength.start();
      rve.getHcw().getBehaviors().put("ROOMEXIT", visitLength);

      // action controller is groovy; has access to methods on the metaclass
      this.actionController.healthCareWorkerVisitActions(rve);
      this.data.setVisitCount(this.data.getVisitCount() + 1);
    }

    if (e instanceof RoomLeave) {
      RoomLeave rle = (RoomLeave) e;
      rle.getHealthCareWorker().getBehaviors().remove("ROOMEXIT");
      rle.getHealthCareWorker().getBehaviors().get("ROOMVISIT").start();
    }
  }

  // Physical Init
  private void calculateCapacity() {
    for (Room r : this.sicu.getRooms()) {
      this.sicu.setCapacity(this.sicu.getCapacity() + r.getCapacity());
    }
    for (Room r : this.tele.getRooms()) {
      this.tele.setCapacity(this.tele.getCapacity() + r.getCapacity());
    }
    for (Room r : this.medsurg.getRooms()) {
      this.medsurg.setCapacity(this.medsurg.getCapacity() + r.getCapacity());
    }
    for (Room r : this.acute.getRooms()) {
      this.acute.setCapacity(this.acute.getCapacity() + r.getCapacity());
    }
    this.hospital.setCapacity(
        this.tele.getCapacity()
            + this.sicu.getCapacity()
            + this.medsurg.getCapacity()
            + this.acute.getCapacity());
    System.out.println("capacity = " + this.hospital.getCapacity());
  }

  private void buildBeds() {
    for (Room r : this.rooms) {
      int bedcount = 1;
      if (!r.isSinglePatient()) {
        bedcount = 2;
      }

      for (int i = 0; i < bedcount; i++) {
        Bed b = new Bed(r);
        r.addBed(b);
        this.hospital.getBeds().add(b);
      }
    }
  }

  private void buildWards() {
    this.sicu = new Ward(this.hospital);
    this.tele = new Ward(this.hospital);
    this.acute = new Ward(this.hospital);
    this.medsurg = new Ward(this.hospital);
    // telem = new Ward();

    this.hospital.addWard(this.sicu);
    this.hospital.addWard(this.tele);
    this.hospital.addWard(this.acute);
    this.hospital.addWard(this.medsurg);
    // hospital.addWard(telem);
  }

  private void buildNurses() {
    // tele nurses
    for (int i = 0; i < 2; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.tele.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.tele, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
      // TODO:  implement InteractVisit:
      /* non-instantaneus,
       * HH, PPE check on entry
       * -depends on visit type, HCW class, isolation status
       * HH check on exit, PPE doff
       *
       */
    }

    // sicu nurses
    for (int i = 0; i < 2; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.sicu.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.sicu, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
    }

    // acute nurses
    for (int i = 0; i < 4; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.acute.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.acute, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
    }
    // medsurg nurses
    for (int i = 0; i < 4; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.medsurg.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.medsurg, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
    }
  }

  private void buildRooms() {

    // create sicu rooms
    Room s1 = new Room("3D11", true, false, false, false, false, this.sicu);
    this.sicu.addRoom(s1);

    Room s2 = new Room("3D13", true, false, false, false, false, this.sicu);
    this.sicu.addRoom(s2);

    Room s3 = new Room("3D15", true, false, false, false, false, this.sicu);
    this.sicu.addRoom(s3);

    Room s4 = new Room("3D17", true, true, true, false, false, this.sicu);
    this.sicu.addRoom(s4);

    Room s5 = new Room("3D19", true, true, true, false, false, this.sicu);
    this.sicu.addRoom(s5);

    Room s6 = new Room("3D21", true, false, false, false, false, this.sicu);
    this.sicu.addRoom(s6);

    Room s7 = new Room("3D23A", true, false, false, true, true, this.sicu);
    this.sicu.addRoom(s7);

    Room s8 = new Room("3D27", true, false, false, false, false, this.sicu);
    this.sicu.addRoom(s8);

    Room s9 = new Room("3D29", true, false, false, false, false, this.sicu);
    this.sicu.addRoom(s9);

    Room s10 = new Room("3D25", true, false, false, true, true, this.sicu);
    this.sicu.addRoom(s10);
    this.rooms.addAll(this.sicu.getRooms());

    // build acute rooms
    this.acute.addRoom(new Room("2A01", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2A02", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2A04", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2A06", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2B11", true, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2B13", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2B14", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2B17", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B18", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B19", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B20", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B24", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B26", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B28", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2C01", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2C02", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2C03", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2C04", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2C05", true, true, false, true, true, this.acute));
    this.acute.addRoom(new Room("2C06", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2C14", true, true, false, true, true, this.acute));
    this.acute.addRoom(new Room("2C16", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2C19", false, true, true, false, false, this.acute));
    this.rooms.addAll(this.acute.getRooms());

    // build medsurg rooms
    this.medsurg.addRoom(new Room("3A01", true, true, false, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A02", false, true, false, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A03", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A04", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A05", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A06", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A07", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A08", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A09", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A17", false, true, false, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A19", true, true, false, true, true, this.medsurg));
    this.medsurg.addRoom(new Room("3A21", true, true, false, true, true, this.medsurg));
    this.medsurg.addRoom(new Room("3A24", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A26", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A29", true, true, false, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A31", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A35", true, true, false, false, false, this.medsurg));
    this.rooms.addAll(this.medsurg.getRooms());

    this.tele.addRoom(new Room("2D11", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D13", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D15", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D19", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D21", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D23", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D24", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D25", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D26", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D27", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D28", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D31", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D33", true, false, false, false, false, this.tele));
    this.tele.addRoom(new Room("2D35", true, false, false, false, false, this.tele));
    this.rooms.addAll(this.tele.getRooms());

    for (Room r : this.rooms) {
      r.addActionListener(this);
    }
  }

  @Override
  public void admit(Patient p) {
    // TODO Auto-generated method stub

  }
}
