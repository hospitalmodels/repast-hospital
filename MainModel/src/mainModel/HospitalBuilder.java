package mainModel;
// test
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import actionevents.PatientAdmitted;
import actionevents.PatientDischarged;
import actionevents.RoomLeave;
import actionevents.RoomVisit;
import agents.Agent;
import agents.Nurse;
import agents.Patient;
import behavior.RoomVisitDuration;
import behavior.StochasticRoomVisit;
import datacollectors.RuntimeData;
import metacomponents.RoomVisitComponent;
import processes.roomcleaning.CleaningShiftStart;
import repast.simphony.context.Context;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduledMethod;
import simple_adt.Admission;
import simple_adt.Discharge;
import structure.Admittable;
import structure.Bed;
import structure.Dischargeable;
import structure.Facility;
import structure.Room;
import structure.Ward;
import util.OutputFile;
import util.TimeUtils;

public class HospitalBuilder
    implements ContextBuilder<Object>, Admittable, Dischargeable, ActionListener {
  private Facility hospital;
  private Ward sicu, micu, acute, medsurg;
  private ArrayList<Room> rooms;
  private Admission admission;
  private Discharge discharger;
  private CleaningShiftStart cleaning;
  private ISchedule schedule;
  private ActionController actionController;
  private RuntimeData data;
  private OutputFile file;

  @ScheduledMethod(start = 1, interval = 1)
  public void daily() {
    this.cleaning.start();
  }

  @ScheduledMethod(start = 90, interval = 1)
  public void endDate() {
    RunEnvironment.getInstance().endRun();
  }

  @Override
  public Context<Object> build(Context<Object> context) {
    String outputHead = "tick,eventid,source,data";
    this.file = new OutputFile(outputHead, "outputs.txt");
    this.file.createFile();
    // Add specific methods to metaClasses
    RoomVisitComponent.configureRoomVisit();

    this.schedule =
        repast.simphony.engine.environment.RunEnvironment.getInstance().getCurrentSchedule();
    this.hospital = new Facility();
    this.rooms = new ArrayList<Room>();
    this.actionController = new ActionController();
    buildWards();
    buildRooms();
    buildBeds();
    calculateCapacity();
    buildNurses();
    this.data = new RuntimeData(this.schedule);
    this.schedule.schedule(this);

    this.admission = new Admission(.1, this);
    this.admission.start();

    this.discharger = new Discharge(5.5, this);

    // cleaning = new CleaningShiftStart(TimeUtils.HOUR*2, TimeUtils.HOUR*22, rooms);
    // cleaning.start();

    this.hospital.setId("MainModel");
    return this.hospital;
  }

  // Process implementations
  @Override
  public void admit() {
    if (this.hospital.getVacancy() > 0) {

      Bed targetBed =
          (Bed)
              this.hospital
                  .getEmptyBeds()
                  .get(new Random().nextInt(this.hospital.getEmptyBeds().size()));
      Patient p = new Patient();
      p.addActionListener(this);
      p.fireActionPerformed(new PatientAdmitted(p));
      targetBed.setPatient(p);
      p.setBed(targetBed);

      this.hospital.add(p);

    } else {
      // no action, hospital is full
    }
  }

  @Override
  public void discharge(Agent a) {
    Patient p = (Patient) a;
    p.getBed().setPatient(null);
    p.setBed(null);
    p.fireActionPerformed(new PatientDischarged(p));
    this.hospital.remove(p);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String outputLine =
        this.schedule.getTickCount()
            + ","
            + e.getID()
            + ","
            + e.getSource().toString()
            + ","
            + e.getActionCommand();
    this.file.writeLine(outputLine);
    // System.out.println(schedule.getTickCount() +   "," + e.getSource().toString() + "," +
    // e.getActionCommand());
    if (e instanceof PatientAdmitted) {
      this.discharger.newPatient(((PatientAdmitted) e).getPatient());
    }
    if (e instanceof PatientDischarged) {
      // do patient discharge things
    }
    if (e instanceof RoomVisit) {
      RoomVisit rve = (RoomVisit) e;
      rve.getHcw().getBehaviors().get("ROOMVISIT").stop();
      RoomVisitDuration visitLength =
          new RoomVisitDuration(rve.getHcw(), rve.getRoom(), 15 * TimeUtils.MINUTE);
      visitLength.start();
      rve.getHcw().getBehaviors().put("ROOMEXIT", visitLength);

      // action controller is groovy; has access to methods on the metaclass
      this.actionController.healthCareWorkerVisitActions(rve);
      this.data.setVisitCount(this.data.getVisitCount() + 1);
    }

    if (e instanceof RoomLeave) {
      RoomLeave rle = (RoomLeave) e;
      rle.getHealthCareWorker().getBehaviors().remove("ROOMEXIT");
      rle.getHealthCareWorker().getBehaviors().get("ROOMVISIT").start();
    }
  }

  // Physical Init
  private void calculateCapacity() {
    for (Room r : this.sicu.getRooms()) {
      this.sicu.setCapacity(this.sicu.getCapacity() + r.getCapacity());
    }
    for (Room r : this.micu.getRooms()) {
      this.micu.setCapacity(this.micu.getCapacity() + r.getCapacity());
    }
    for (Room r : this.medsurg.getRooms()) {
      this.medsurg.setCapacity(this.medsurg.getCapacity() + r.getCapacity());
    }
    for (Room r : this.acute.getRooms()) {
      this.acute.setCapacity(this.acute.getCapacity() + r.getCapacity());
    }
    this.hospital.setCapacity(
        this.micu.getCapacity()
            + this.sicu.getCapacity()
            + this.medsurg.getCapacity()
            + this.acute.getCapacity());
    System.out.println("capacity = " + this.hospital.getCapacity());
  }

  private void buildBeds() {
    for (Room r : this.rooms) {
      int bedcount = 1;
      if (!r.isSinglePatient()) {
        bedcount = 2;
      }

      for (int i = 0; i < bedcount; i++) {
        Bed b = new Bed(r);
        r.addBed(b);
        this.hospital.getBeds().add(b);
      }
    }
  }

  private void buildWards() {
    this.sicu = new Ward(this.hospital);
    this.micu = new Ward(this.hospital);
    this.acute = new Ward(this.hospital);
    this.medsurg = new Ward(this.hospital);
    // telem = new Ward();

    this.hospital.addWard(this.sicu);
    this.hospital.addWard(this.micu);
    this.hospital.addWard(this.acute);
    this.hospital.addWard(this.medsurg);
    // hospital.addWard(telem);
  }

  private void buildNurses() {
    // micu nurses
    for (int i = 0; i < 2; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.micu.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.micu, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
    }

    // sicu nurses
    for (int i = 0; i < 2; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.sicu.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.sicu, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
    }

    // acute nurses
    for (int i = 0; i < 4; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.acute.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.acute, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
    }
    // medsurg nurses
    for (int i = 0; i < 4; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.medsurg.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.medsurg, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
    }
  }

  private void buildRooms() {

    // create sicu rooms
    Room s1 = new Room("3D11", true, false, false, false, false, this.sicu);
    this.sicu.addRoom(s1);

    Room s2 = new Room();
    s2.setRoomNumber("3D13");
    s2.setSinglePatient(true);
    s2.setBathroom(false);
    s2.setNegativePressure(false);
    s2.setWard(this.sicu);
    this.sicu.addRoom(s2);

    Room s3 = new Room();
    s3.setRoomNumber("3D15");
    s3.setSinglePatient(true);
    s3.setBathroom(false);
    s3.setNegativePressure(false);
    s3.setWard(this.sicu);
    this.sicu.addRoom(s3);

    Room s4 = new Room();
    s4.setRoomNumber("3D17");
    s4.setSinglePatient(true);
    s4.setBathroom(true);
    s4.setSharedBathroom(true);
    s4.setNegativePressure(false);
    s4.setWard(this.sicu);
    this.sicu.addRoom(s4);

    Room s5 = new Room();
    s5.setRoomNumber("3D19");
    s5.setSinglePatient(true);
    s5.setBathroom(true);
    s5.setSharedBathroom(true);
    s5.setNegativePressure(false);
    s5.setWard(this.sicu);
    this.sicu.addRoom(s5);

    Room s6 = new Room();
    s6.setRoomNumber("3D21");
    s6.setSinglePatient(true);
    s6.setBathroom(false);
    s6.setWard(this.sicu);
    this.sicu.addRoom(s6);

    Room s7 = new Room();
    s7.setRoomNumber("3D23A");
    s7.setSinglePatient(true);
    s7.setBathroom(false);
    s7.setNegativePressure(true);
    s7.setIsolation(true);
    s7.setWard(this.sicu);
    this.sicu.addRoom(s7);

    Room s8 = new Room();
    s8.setRoomNumber("3D27");
    s8.setSinglePatient(true);
    s8.setBathroom(false);
    s8.setWard(this.sicu);
    this.sicu.addRoom(s8);

    Room s9 = new Room("3D29", true, false, false, false, false, this.sicu);
    this.sicu.addRoom(s9);

    this.rooms.addAll(this.sicu.getRooms());

    // build acute rooms
    this.acute.addRoom(new Room("2A01", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2A02", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2A04", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2A06", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2B11", true, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2B13", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2B14", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2B17", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B18", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B19", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B20", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B24", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B26", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2B28", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2C01", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2C02", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2C03", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2C04", false, true, true, false, false, this.acute));
    this.acute.addRoom(new Room("2C05", true, true, false, true, true, this.acute));
    this.acute.addRoom(new Room("2C06", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2C14", true, true, false, true, true, this.acute));
    this.acute.addRoom(new Room("2C16", true, true, false, false, false, this.acute));
    this.acute.addRoom(new Room("2C19", false, true, true, false, false, this.acute));
    this.rooms.addAll(this.acute.getRooms());

    // build medsurg rooms
    this.medsurg.addRoom(new Room("3A01", true, true, false, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A02", false, true, false, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A03", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A04", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A05", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A06", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A07", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A08", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A09", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A17", false, true, false, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A19", true, true, false, true, true, this.medsurg));
    this.medsurg.addRoom(new Room("3A21", true, true, false, true, true, this.medsurg));
    this.medsurg.addRoom(new Room("3A24", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A26", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A29", true, true, false, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A31", false, true, true, false, false, this.medsurg));
    this.medsurg.addRoom(new Room("3A35", true, true, false, false, false, this.medsurg));
    this.rooms.addAll(this.medsurg.getRooms());

    for (Room r : this.rooms) {
      r.addActionListener(this);
    }
  }

  @Override
  public void admit(Patient p) {
    // TODO Auto-generated method stub

  }
}
