package mainModel;
// test
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import actionevents.PatientAdmitted;
import actionevents.PatientDischarged;
import actionevents.RoomLeave;
import actionevents.RoomVisit;
import agents.Agent;
import agents.Nurse;
import agents.Patient;
import behavior.RoomVisitDuration;
import behavior.StochasticRoomVisit;
import datacollectors.RuntimeData;
import metacomponents.RoomVisitComponent;
import repast.simphony.context.Context;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduledMethod;
import simple_adt.Admission;
import simple_adt.Discharge;
import structure.Admittable;
import structure.Bed;
import structure.Dischargeable;
import structure.Facility;
import structure.Room;
import structure.Ward;
import util.OutputFile;
import util.TimeUtils;

public class InteractBuilder
    implements ContextBuilder<Object>, Admittable, Dischargeable, ActionListener {
  private Facility hospital;
  private Ward icu, tele, threewest, twoeast;
  private ArrayList<Room> rooms;
  private Admission admission;
  private Discharge discharger;
  private ISchedule schedule;
  private ActionController actionController;
  private RuntimeData data;
  private OutputFile file;
  private ArrayList<Patient> nonIcuPatients;
  public static int CAPACITY = 100;

  @ScheduledMethod(start = 1, interval = 1)
  public void daily() {}

  @ScheduledMethod(start = 90, interval = 1)
  public void endDate() {
    RunEnvironment.getInstance().endRun();
  }

  @Override
  public Context<Object> build(Context<Object> context) {
    String outputHead = "tick,eventid,source,data";
    this.file = new OutputFile(outputHead, "outputs.txt");
    this.file.createFile();
    // Add specific methods to metaClasses
    RoomVisitComponent.configureRoomVisit();

    this.schedule =
        repast.simphony.engine.environment.RunEnvironment.getInstance().getCurrentSchedule();
    this.hospital = new Facility();
    this.rooms = new ArrayList<Room>();
    this.nonIcuPatients = new ArrayList<Patient>();
    this.actionController = new ActionController();
    buildWards();
    buildRooms();
    buildBeds();
    calculateCapacity();
    buildNurses();
    // buildCnas();
    // buildPts();
    // buildOts();
    // buildRts();
    // buildMds();
    // buildWardMds();
    // buildPharmacists();
    this.data = new RuntimeData(this.schedule);
    this.schedule.schedule(this);

    this.admission = new Admission(.1, this);
    this.admission.start();

    // TODO:  Build transfer system
    // When ward stay is over, there's a transfer probability
    // matrix that determines flow to next ward.
    // this is is competition with the discharge timer

    this.discharger = new Discharge(5.5, this);

    // cleaning = new CleaningShiftStart(TimeUtils.HOUR*2, TimeUtils.HOUR*22, rooms);
    // cleaning.start();

    this.hospital.setId("MainModel");
    return this.hospital;
  }

  // Process implementations
  @Override
  public void admit() {
    if (this.hospital.getVacancy() > 0) {

      Bed targetBed =
          (Bed)
              this.hospital
                  .getEmptyBeds()
                  .get(new Random().nextInt(this.hospital.getEmptyBeds().size()));
      Patient p = new Patient();
      p.addActionListener(this);
      p.fireActionPerformed(new PatientAdmitted(p));
      targetBed.setPatient(p);
      p.setBed(targetBed);

      this.hospital.add(p);

    } else {
      // no action, hospital is full
    }
  }

  @Override
  public void discharge(Agent a) {
    Patient p = (Patient) a;
    p.getBed().setPatient(null);
    p.setBed(null);
    p.fireActionPerformed(new PatientDischarged(p));
    this.hospital.remove(p);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    String outputLine =
        this.schedule.getTickCount()
            + ","
            + e.getID()
            + ","
            + e.getSource().toString()
            + ","
            + e.getActionCommand();
    this.file.writeLine(outputLine);
    // System.out.println(schedule.getTickCount() +   "," + e.getSource().toString() + "," +
    // e.getActionCommand());
    if (e instanceof PatientAdmitted) {
      this.discharger.newPatient(((PatientAdmitted) e).getPatient());
    }
    if (e instanceof PatientDischarged) {
      // do patient discharge things
    }
    if (e instanceof RoomVisit) {
      RoomVisit rve = (RoomVisit) e;
      rve.getHcw().getBehaviors().get("ROOMVISIT").stop();
      RoomVisitDuration visitLength =
          new RoomVisitDuration(rve.getHcw(), rve.getRoom(), 15 * TimeUtils.MINUTE);
      visitLength.start();
      rve.getHcw().getBehaviors().put("ROOMEXIT", visitLength);

      // action controller is groovy; has access to methods on the metaclass
      this.actionController.healthCareWorkerVisitActions(rve);
      this.data.setVisitCount(this.data.getVisitCount() + 1);
    }

    if (e instanceof RoomLeave) {
      RoomLeave rle = (RoomLeave) e;
      rle.getHealthCareWorker().getBehaviors().remove("ROOMEXIT");
      rle.getHealthCareWorker().getBehaviors().get("ROOMVISIT").start();
    }
  }

  // Physical Init
  private void calculateCapacity() {
    for (Room r : this.icu.getRooms()) {
      this.icu.setCapacity(this.icu.getCapacity() + r.getCapacity());
    }
    for (Room r : this.tele.getRooms()) {
      this.tele.setCapacity(this.tele.getCapacity() + r.getCapacity());
    }
    for (Room r : this.threewest.getRooms()) {
      this.threewest.setCapacity(this.threewest.getCapacity() + r.getCapacity());
    }
    for (Room r : this.twoeast.getRooms()) {
      this.twoeast.setCapacity(this.twoeast.getCapacity() + r.getCapacity());
    }
    this.hospital.setCapacity(
        this.tele.getCapacity()
            + this.icu.getCapacity()
            + this.threewest.getCapacity()
            + this.twoeast.getCapacity());
    System.out.println("capacity = " + this.hospital.getCapacity());
  }

  private void buildBeds() {
    for (Room r : this.rooms) {
      int bedcount = 1;
      if (!r.isSinglePatient()) {
        bedcount = 2;
      }

      for (int i = 0; i < bedcount; i++) {
        Bed b = new Bed(r);
        r.addBed(b);
        this.hospital.getBeds().add(b);
      }
    }
  }

  private void buildWards() {
    this.icu = new Ward(this.hospital);
    this.tele = new Ward(this.hospital);
    this.threewest = new Ward(this.hospital);
    this.twoeast = new Ward(this.hospital);
    // telem = new Ward();

    this.hospital.addWard(this.icu);
    this.hospital.addWard(this.tele);
    this.hospital.addWard(this.threewest);
    this.hospital.addWard(this.twoeast);
    // hospital.addWard(telem);
  }

  private void buildNurses() {
    // tele nurses
    for (int i = 0; i < 6; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.tele.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.tele, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
      // TODO:  implement InteractVisit:
      /* non-instantaneus,
       * HH, PPE check on entry
       * -depends on visit type, HCW class, isolation status
       * HH check on exit, PPE doff
       *
       */
    }

    // sicu nurses
    for (int i = 0; i < 8; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.icu.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.icu, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
    }

    // acute nurses
    for (int i = 0; i < 8; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.twoeast.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(n, this.twoeast, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
    }
    // medsurg nurses
    for (int i = 0; i < 8; i++) {
      Nurse n = new Nurse();
      this.hospital.add(n);
      this.threewest.getNurses().add(n);
      n.addActionListener(this);
      n.getBehaviors()
          .put(
              "ROOMVISIT",
              new StochasticRoomVisit(
                  n, this.threewest, TimeUtils.HOUR * 4, TimeUtils.MINUTE * 12));
      n.getBehaviors().get("ROOMVISIT").start();
    }
  }

  private void buildRooms() {
    for (int i = 0; i < 10; i++) {
      this.icu.addRoom(new Room("icu" + i, true, true, false, false, false, this.icu));
    }
    this.rooms.addAll(this.icu.getRooms());

    // build acute rooms
    for (int i = 0; i < 21; i++) {
      this.tele.addRoom(new Room("tele" + i, true, true, false, false, false, this.tele));
    }
    this.rooms.addAll(this.tele.getRooms());

    // build medsurg rooms
    for (int i = 0; i < 40; i++) {
      this.twoeast.addRoom(new Room("twoeast" + i, true, true, false, false, false, this.twoeast));
    }
    this.rooms.addAll(this.twoeast.getRooms());

    for (int i = 0; i < 27; i++) {
      this.threewest.addRoom(
          new Room("threewest" + i, true, true, false, false, false, this.threewest));
    }
    this.rooms.addAll(this.threewest.getRooms());

    for (Room r : this.rooms) {
      r.addActionListener(this);
    }
  }

  @Override
  public void admit(Patient p) { // TODO Auto-generated method stub
  }
}
