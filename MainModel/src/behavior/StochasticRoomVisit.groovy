package behavior

import java.util.Random



import agents.HealthCareWorker
import agents.Patient;
import processes.Process
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ISchedulableAction;
import repast.simphony.engine.schedule.ScheduleParameters
import structure.Bed
import structure.Room;
import structure.Ward
import util.TimeUtils;

class StochasticRoomVisit extends Process{
	HealthCareWorker hcw
	Ward ward
	
	double intraPerPatient
	double meanVisitDuration
	ISchedulableAction nextVisit
	boolean stop=false
	
	
	StochasticRoomVisit(HealthCareWorker hcw, Ward ward, double intraPerPatient, double meanVisitDuration){
		super(intraPerPatient)
		this.hcw=hcw
		this.ward=ward
		this.intraPerPatient = intraPerPatient
		this.meanVisitDuration = meanVisitDuration
	}
	
	def start(){
		stop=false
		double currentTime = schedule.getTickCount()
		double timeToElapse = TimeUtils.MINUTE*5
		
		
		if (ward.getPatients().size()>0){
			timeToElapse = distro.sample()/ward.getPatients().size()
			//println("patients detected.  time to next visit: " + timeToElapse)
		} 
		//println("base intra: " + intraPerPatient + ", pt count:" + ward.getPatients().size() + ", actual intra: " + timeToElapse)
		double eventTime = currentTime+timeToElapse
		ScheduleParameters schedParams = ScheduleParameters.createOneTime((eventTime))
		nextVisit = schedule.schedule(schedParams, this, "visit")
	}
	
	def visit(){
		if (stop){
			return
		}
		
		nextVisit = null
		if (ward.getPatients().size()==0){
			
			
			start()
			return
		}
		//RunEnvironment.getInstance().pauseRun()
		
		//find patient at random
		Patient p = ward.getPatients().get(new Random().nextInt(ward.getPatients().size()))
		Bed b = p.bed
		Room room = b.room
		
		assert p
		assert b
		
		room.visit(hcw, p)
		
		
		
		
		
		
	}
	
	
	
	def stop(){
		stop=true
		//schedule.removeAction(nextVisit)
	}
	
	

}
