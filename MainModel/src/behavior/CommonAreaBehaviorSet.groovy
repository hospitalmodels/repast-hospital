package behavior

import org.apache.commons.math3.distribution.ExponentialDistribution

import agents.HealthCareWorker
import mainModel.GranularBuilder
import repast.simphony.engine.schedule.ScheduleParameters
import repast.simphony.space.graph.Network
import util.TimeUtils
import processes.Process

class CommonAreaBehaviorSet extends Process {
	private GranularBuilder main
	private HealthCareWorker hcw
	private Network<Object> common
	
	public CommonAreaBehaviorSet(double intra, HealthCareWorker hcw, GranularBuilder main, Network<Object> common) {
		super(intra)
		this.main = main
		this.hcw = hcw
		this.common = common;
	}
	
	@Override
	public Object start() {
		
		double currentTime = schedule.getTickCount()
		ExponentialDistribution mixDistro = new ExponentialDistribution(0.25)
		double timeToElapse = mixDistro.sample()
		double eventTime = currentTime+timeToElapse
		ScheduleParameters schedParams = ScheduleParameters.createOneTime((eventTime))
		schedule.schedule(schedParams, this, "mix")
		
	}
	
	public void mix() {
		if (hcw.getInCommonArea() == true) {
			main.event("COMMONMIX", hcw.toString(), null, null, "")
		}
		start()
	}

	@Override
	public Object stop() {
		return null
	}

}
