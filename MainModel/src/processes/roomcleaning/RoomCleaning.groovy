package processes.roomcleaning

import java.awt.event.ActionEvent
import java.awt.event.ActionListener

import processes.Process
import repast.simphony.engine.schedule.ScheduleParameters
import structure.Room;;

class RoomCleaning extends Process implements ActionListener{
	double startTime
	double endTime
	Room room

	public RoomCleaning(double intra,double startTime,double EndTime, Room room) {
		super(intra);
		startTime = startTime
		endTime = endTime
		room = room
	}

	@Override
	public Object start() {
		def currTime = schedule.getTickCount()
		def elapse = distro.sample()
		def eventTime = currTime+elapse
		ScheduleParameters params= ScheduleParameters.createOneTime(eventTime)
		schedule.schedule(params, this, "clean")
		return null;
	}
	
	public void clean(){
		
		
		start()
	}

	@Override
	public Object stop() {
		return null;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
