package processes.roomcleaning

import actionevents.RoomCleaningEvent
import mainModel.GranularBuilder
import processes.Process
import repast.simphony.engine.schedule.ScheduleParameters;
import structure.Room;

class CleaningShiftStart extends Process {
    double delay //how long to wait before starting the cleaning shift
    double shiftLength //how long the shift will last
    double intraCleaningTime
    ArrayList<Room> rooms
    GranularBuilder main





    public CleaningShiftStart( double shiftLength, double shiftDelay, List<Room> rooms, GranularBuilder main) {
	super(shiftLength/rooms.size());
	this.shiftLength = shiftLength
	this.delay = shiftDelay
	this.rooms = rooms
	this.intraCleaningTime = shiftLength/rooms.size()
	this.main = main
    }

    @Override
    public Object start() {
	double startAt = this.delay+schedule.getTickCount()
	for (int i=0; i<this.rooms.size(); i++) {

	    double eventTime = startAt + i*intraCleaningTime
	    ScheduleParameters params = ScheduleParameters.createOneTime(eventTime)
	    schedule.schedule(params, this, "cleanRoom", rooms[i])
	}
	return null;
    }


    def cleanRoomOnce(Room r){

	r.clean()
	fireActionPerformed(new RoomCleaningEvent(r, "POSTVISIT"))
	main.event("CLEAN", "POSTVISIT" + r.toString(), null, null, "")
    }

    def cleanRoom(Room r){
	r.clean()
	fireActionPerformed(new RoomCleaningEvent(r, "REGULAR"))
	main.event("CLEAN", "REGULAR" + r.toString(), null, null, "")
    }

    def terminal(Room r) {
	r.clean()
	fireActionPerformed(new RoomCleaningEvent(r, "TERMINAL"))
	main.event("CLEAN", "TERMINAL" + r.toString(), null, null, "")
    }



    public Object stop() {
	return null;
    }
}
