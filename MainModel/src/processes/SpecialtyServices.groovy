package processes

import agents.Patient
import util.Chooser
import util.InputData

class SpecialtyServices  {
    public ArrayList<Patient> needsPT
    public ArrayList<Patient> needsRT

    double ptFraction,rtFraction


    public void newPatient(Patient p) {
	if (Chooser.randomTrue(ptFraction)){
	    needsPT.add(p)
	}
	if (Chooser.randomTrue(rtFraction)) {
	    needsRT.add(p)
	}
    }

    public void discharge(Patient p) {
	needsPT.remove(p)
	needsRT.remove(p)
    }

    public Patient getOnePTPatient() {
	return Chooser.chooseOne(needsPT)
    }

    public Patient getOneRTPatient() {
	return Chooser.chooseOne(needsRT)
    }

    public SpecialtyServices(InputData data) {
	needsPT = new ArrayList<Patient>()
	needsRT = new ArrayList<Patient>()
	ptFraction = data.get("fraction_of_admits_need_pt")
	rtFraction = data.get("fraction_of_admits_need_rt")
    }
}
