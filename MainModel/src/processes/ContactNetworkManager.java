package processes;

import java.util.ArrayList;
import java.util.LinkedList;

import agents.Agent;
import agents.AgentType;
import agents.HealthCareWorker;
import agents.Patient;
import granular.behavior.NurseAssignedVisit;
import mainModel.GranularBuilder;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.space.graph.Network;
import structure.Ward;

public class ContactNetworkManager extends Process {
  private LinkedList<HealthCareWorker> hcws;
  private Ward ward;
  private double shiftRepeat = 0.5;
  private GranularBuilder main;
  private Network<Object> assigned;
  private boolean stopped;
  private ISchedule schedule;
  private int nextHcwIndex = 0;

  public ContactNetworkManager(
      double intra,
      ArrayList<HealthCareWorker> hcws,
      Ward w,
      GranularBuilder main,
      Network<Object> assigned) {
    super(intra);
    this.assigned = assigned;
    this.hcws = new LinkedList<HealthCareWorker>();
    for (HealthCareWorker h : hcws) {
      this.hcws.add(h);
    }

    this.ward = w;
    this.main = main;
    this.stopped = true;
    this.schedule =
        repast.simphony.engine.environment.RunEnvironment.getInstance().getCurrentSchedule();
  }

  @Override
  public Object start() {
    double currTime = this.schedule.getTickCount();
    double eventTime = currTime + this.shiftRepeat;
    ScheduleParameters params = ScheduleParameters.createOneTime(eventTime);
    this.schedule.schedule(params, this, "assignEdges");
    return this;
  }

  public void assignEdges() {
    NurseAssignedVisit nav = null;
    this.main.assigned.removeEdges();
    for (HealthCareWorker n : this.main.nurses) {

      nav = (NurseAssignedVisit) n.getBehaviors().get("assigned_visits");
      if (nav != null) {
        n.getAssignedPatients().clear();
      }
    }

    int hcwCount = this.hcws.size();

    for (Patient p : this.main.patients) {
      HealthCareWorker h = this.hcws.get(this.nextHcwIndex++ % hcwCount);

      this.main.assigned.addEdge(h, p);

      if (nav != null) {

        h.getAssignedPatients().add(p);
      }

      // System.out.println("Nurse degree = " + this.assigned.getDegree(h));

      this.main.event(
          "ASSIGNMENT",
          h.toString() + " assigned to " + p.toString(),
          h,
          p,
          p.getAgentId() + "," + p.getRoom().getAgentId());
    }

    start();
  }

  public void newPatient(Agent a) {
    Patient p = (Patient) a;
    Agent target = this.hcws.get(this.nextHcwIndex++ % this.hcws.size());
    HealthCareWorker h = (HealthCareWorker) target;
    if (h.getType() != AgentType.NURSE) {
      h.getAssignedPatients().add(p);
      this.main.assigned.addEdge(target, p);
      this.main.event(
          "ASSIGNMENT",
          target.toString() + " assigned to " + p.toString(),
          target,
          a,
          p.getAgentId() + "," + p.getRoom().getAgentId());
      // h.getBehaviors().get("assigned_visit").start();
      // h.getBehaviors().get("random_visit").start();
    }
  }

  public Agent getOneLeastUtilized(ArrayList<HealthCareWorker> hcws) {
    int least = 1000;
    for (Agent hc : hcws) {
      least = Math.min(least, this.assigned.getDegree(hc));
    }
    ArrayList<HealthCareWorker> leastUtilized = new ArrayList<HealthCareWorker>();
    for (HealthCareWorker hc : hcws) {
      if (hc.getAssignedPatientCount() == least) {
        leastUtilized.add(hc);
      }
    }
    int rando = (int) (Math.random() * leastUtilized.size());
    return hcws.get(rando);
  }

  @Override
  public Object stop() {
    this.stopped = true;
    return null;
  }
}
