package simple_adt

import org.apache.commons.math3.distribution.ExponentialDistribution
import org.apache.commons.math3.distribution.LogNormalDistribution

import processes.*
import repast.simphony.engine.schedule.ScheduleParameters
import structure.Admittable;
import structure.Facility;
import repast.simphony.random.RandomHelper
import cern.jet.random.AbstractDistribution

class Admission extends Process{
	Admittable target
	LogNormalDistribution newDistro
	
	
	Admission(double meanIET, Admittable target){
		super(meanIET)
		this.target = target	
		this.meanIntraEventTime = meanIET
		distro = new ExponentialDistribution(meanIET)
	}
	

	def start(){
		double currentTime = schedule.getTickCount()
		double timeToElapse = distro.sample()
		double eventTime = currentTime+timeToElapse
		
		ScheduleParameters schedParams = ScheduleParameters.createOneTime((eventTime))
		nextAdmissionAction = schedule.schedule(schedParams, this, "action")
		
		
		
		return eventTime
	}
	
	def action(){
		target.admit()
		start()
	}
	
	def stop(){
		
	}

}
