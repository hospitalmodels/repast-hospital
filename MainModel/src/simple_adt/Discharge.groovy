package simple_adt

import org.apache.commons.math3.distribution.LogNormalDistribution

import agents.Patient;
import processes.Process
import repast.simphony.engine.schedule.ISchedulableAction;
import repast.simphony.engine.schedule.ScheduleParameters
import structure.Dischargeable

class Discharge extends Process {
    double meanLOS
    Dischargeable target
    LogNormalDistribution newDistro

    Discharge(double meanLOS, Dischargeable target){
	super(meanLOS)
	//TODO:  Seriously.
	newDistro = new LogNormalDistribution(0.7032373, 0.9986254)
	this.target=target
    }

    def newPatient(Patient p){
	double currentTime = schedule.getTickCount()
	double timeToElapse = newDistro.sample()
	double eventTime = currentTime+timeToElapse
	ScheduleParameters schedParams = ScheduleParameters.createOneTime((eventTime))
	ISchedulableAction discharge = schedule.schedule(schedParams, target, "discharge", p)
	p.getBehaviors().put("discharge", discharge)
    }

    def start(){
    }

    def stop(){
    }
}
