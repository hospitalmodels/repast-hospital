package granular.behavior;

import agents.Patient;

public interface IVisit {
  public boolean doVisit();

  public Patient choosePatient();
}
