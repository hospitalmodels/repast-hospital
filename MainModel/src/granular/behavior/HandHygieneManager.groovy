package granular.behavior

import agents.HealthCareWorker
import agents.Patient
import repast.simphony.engine.environment.RunEnvironment
import structure.Room
import util.Chooser

class HandHygieneManager {



    public enum Types {
	GEL,SOAP
    }

    int scenario

    public HandHygieneManager() {
	this.scenario = RunEnvironment.getInstance().getParameters().getInteger("scenario")
    }



    public checkHandHygeine(Vtype eventType, HealthCareWorker hcw, Room room, Patient p, boolean beforeVisit ) {
	double handHygieneIntervention = RunEnvironment.getInstance().getParameters().getDouble("handHygieneIntervention")
	//println(handHygieneIntervention)
	double ret = 0.0
	if (p.getProperty("isolated")==null || p.getProperty("isolated") == false) {
	    if (beforeVisit) {
		try {
		    //ret = HAND_HYGIENE_ADHERENCE[hcw.type]['BEFORE'][eventType.type]
		    String key = "type_" + eventType.type.name().toLowerCase() + "_hh_entry_percent"
		    ret = hcw.get(key)
		}
		catch (Exception e) {
		    println(e.toString())
		}

	    } else {
		//ret HAND_HYGIENE_ADHERENCE[hcw.type]['AFTER'][eventType]
		String key = "type_" + eventType.type.name().toLowerCase() + "_hh_exit_percent"
		ret = hcw.get(key)
	    }
	}

	if (p.getProperty("isolated")) {
	    if (beforeVisit) {

		String key = "type_" + eventType.type.name().toLowerCase() + "_cp_hh_entry_percent"
		ret = hcw.get(key)
		//				 ret = HAND_HYGIENE_ADHERENCE[hcw.type]['ISOLATION_BEFORE'][eventType]
	    } else {
		//ret =  HAND_HYGIENE_ADHERENCE[hcw.type]['ISOLATION_AFTER'][eventType]
		String key = "type_" + eventType.type.name().toLowerCase() + "_cp_hh_exit_percent"
		ret = hcw.get(key)

	    }
	}





	def result = Chooser.randomTrue(ret)

	return result
    }

    public getType(Patient p) {
	def gel_fraction = 0.9

	if (p.getProperty("isolated")){
	    gel_fraction = 0.6
	}

	return Chooser.choose([Types.GEL, Types.SOAP], [gel_fraction, 1-gel_fraction])


    }

    public int getScene() {
	//println("end" + scenario)
	return this.scenario
    }



}
