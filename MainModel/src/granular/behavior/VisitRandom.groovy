package granular.behavior


import actionevents.ContaminationTransferEvent
import actionevents.RoomLeave
import actionevents.RoomVisit
import actionevents.TouchSurfaceAction
import actionevents.VisitSummaryEvent
import actionevents.WashHandsEvent
import agents.Agent
import agents.HealthCareWorker
import agents.Patient
import cern.jet.random.Gamma
import mainModel.GranularBuilder
import processes.Process
import repast.simphony.engine.environment.RunEnvironment
import repast.simphony.engine.schedule.ISchedulableAction
import repast.simphony.engine.schedule.ScheduleParameters
import repast.simphony.parameter.Parameters
import structure.Room
import structure.Surface
import util.Chooser
import util.TimeUtils

class VisitRandom extends Process implements IVisit{
    private Gamma distribution
    private HealthCareWorker hcw
    private ISchedulableAction visitAction, leaveAction
    public List patientCollection;
    private ArrayList visitTypeProbs
    private ArrayList visitTypeChoices
    private GranularBuilder main
    private HandHygieneManager hhManager
    private Surface _HCWMAINSURFACE
    private repast.simphony.parameter.Parameters params
    private double ppeEfficacy, ppeAdherence
    private boolean universalPpe
    private double enteringLoad
    private static util.InputData inputs
    private ArrayList vTypeChoices
    private HashMap visitTypes
    private boolean stopped
    private agents.Patient patient
    private boolean randomVisit = true




    public VisitRandom(Gamma distro, HealthCareWorker hcw, GranularBuilder main) {
	super(0)
	this.distribution = distro
	this.hcw = hcw
	this.main = main
	this.visitTypes = main.vTypes
	this.hhManager = new HandHygieneManager()
	this.params = main.params
	this.inputs = main.inputs
	this.ppeAdherence = params.getDouble("ppeIsolationAdherenceProb")
	this.ppeEfficacy = params.getDouble("ppeEfficacy")
	this.universalPpe = params.getBoolean("universalPpe")
	this.params = main.params
	this.patientCollection = main.patients
	this.schedule = TimeUtils.getSchedule()


	def hcw_type_prefix = hcw.type.name() + "_"
	this.visitTypeProbs = new ArrayList()
	this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_one_visit_prob"))
	this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_two_visit_prob"))
	this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_three_visit_prob"))
	this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_four_visit_prob"))
	this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_five_visit_prob"))

	this.vTypeChoices = new ArrayList<Vtype>()
	this.vTypeChoices.add(visitTypes.get(Vtype.TYPE.ONE))
	this.vTypeChoices.add(visitTypes.get(Vtype.TYPE.TWO))
	this.vTypeChoices.add(visitTypes.get(Vtype.TYPE.THREE))
	this.vTypeChoices.add(visitTypes.get(Vtype.TYPE.FOUR))
	this.vTypeChoices.add(visitTypes.get(Vtype.TYPE.FIVE))
    }

    public void setVisitTypeProbs(ArrayList<Double> probs) {
	this.visitTypeProbs = probs
    }

    public void setIsRandom(boolean r) {
	this.randomVisit = r;
    }


    @Override
    public boolean doVisit(){
	if (patientCollection.size()> 0 && hcw.inCommonArea) {
	    this.patient = choosePatient()
	    this.visit(this.hcw, this.patient)
	} else {
	    start()
	}
    }

    @Override
    public Object start() {
	this.stopped = false
	double tte=this.distribution.nextDouble()*(1.0/24.0)*(1.0/60.0)
	double current = schedule.getTickCount()
	ScheduleParameters sp = ScheduleParameters.createOneTime(tte+current)
	schedule.schedule(sp, this, "doVisit")
    }

    @Override
    public Object stop() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public Patient choosePatient() {
	return (Patient) Chooser.chooseOne(this.patientCollection)
    }

    public VisitSummaryEvent visit(HealthCareWorker hcw, Patient p) {
	VisitSummaryEvent summary = new VisitSummaryEvent(hcw, p, schedule.getTickCount())
	summary.isolation = checkIsolation(p)
	summary.visitType = chooseVisitType(hcw, main).type
	summary.setIsRandom(this.randomVisit)
	summary.end = scheduleVisitEnd(summary)
	calculateTouches(summary)
	recordEntryEvent(summary, main)
	hcw.setInCommonArea(false)
	Room r = p.getRoom()
	fireActionPerformed(new RoomVisit(hcw, r, p, main.vTypes.get(summary.visitType), "visit"))
	r.visit(hcw,p)
	summary.startHH = washHandsOnEntry(summary)
	boolean ppe = willWearPpe(summary)
	if (ppe) {
	    donPpe(summary)
	}
	summary.initPatientLoad = p.getProperty("surface").getLoad(schedule.getTickCount())
	scheduleTouches(summary)
    }

    public boolean checkIsolation(Patient p) {
	if (p.getProperty("isolated") == null) {
	    return false
	}
	if (p.getProperty("isolated") == true) {
	    return true
	} else {
	    return false
	}
    }

    public Vtype chooseVisitType(HealthCareWorker hcw, GranularBuilder gb) {


	Vtype ret = Chooser.choose(this.vTypeChoices, this.visitTypeProbs)
	return ret
    }

    public double scheduleVisitEnd(VisitSummaryEvent sum) {
	//calculate visit duration based on visit type
	Vtype.TYPE vtype = sum.visitType
	def duration = main.vTypes.get(vtype).durationDistribution.nextDouble() //in minutes
	duration *= 1/60 //hours
	duration *= 1/24 //days
	double departureTime = schedule.getTickCount()+duration

	//schedule departure event
	ScheduleParameters sp = ScheduleParameters.createOneTime(departureTime)
	leaveAction = schedule.schedule(sp, this, "leave", sum)

	return departureTime
    }

    public void calculateTouches(VisitSummaryEvent sum) {
	Vtype vtype = main.vTypes.get(sum.visitType)
	sum.nearTouches = vtype.near.nextInt()
	sum.farTouches = vtype.far.nextInt()
	sum.patientTouches = vtype.near.nextInt()//TODO: implement patient touch distribution
	sum.independent_hh = vtype.independent_hh.nextInt()
    }

    public void recordEntryEvent(VisitSummaryEvent sum, GranularBuilder gb) {
	String randomv = "VISIT"
	String worker = hcw.toString()
	double workerStartLoad = sum.hcw.getProperty("surface").getLoad(schedule.getTickCount())
	sum.setInitHcwLoad(workerStartLoad)
	String target = sum.patient.toString()
	String rm = sum.patient.getRoom().toString()
	String vistype = sum.visitType.toString()
	int near = sum.nearTouches
	int far = sum.farTouches
	int patient = sum.patientTouches
	int independent = sum.independent_hh
	String value = worker + " (load: " + workerStartLoad + ") to " + target + " in " + rm + " (" + vistype + " " + near_touches + "n/" + far_touches+"f"
	gb.event(randomv, value, (Agent)sum.hcw, (Agent)sum.patient, "")
    }

    public boolean washHandsOnEntry(VisitSummaryEvent sum) {
	hcw.handHygeineOpportunities += 1
	boolean success = this.hhManager.checkHandHygeine(main.vTypes.get(sum.visitType), sum.hcw, sum.patient.room, sum.patient, true)
	if (success) {
	    hcw.handHygeieneEvents += 1
	    HandHygieneManager.Types type = hhManager.getType(sum.patient)
	    fireActionPerformed(new WashHandsEvent(sum.hcw, type))
	}
	return success
    }

    public boolean willWearPpe(VisitSummaryEvent sum) {
	if (this.universalPpe) {
	    return true
	}
	if (sum.isolation) {
	    if (Chooser.randomTrue(this.ppeAdherence)) {
		return true
	    }
	}
	return false
    }

    public void donPpe(VisitSummaryEvent sum) {
	//store HCW's personal surface
	_HCWMAINSURFACE = sum.hcw.getProperty("surface")
	//create the new surface of the ppe
	Surface ppeSurface = new Surface(sum.hcw.toString(),1, schedule.getTickCount())
	//calculate how much the hcw contaminates the ppe in the process of donning it
	double leakage = _HCWMAINSURFACE.getLoad(schedule.getTickCount()) * (1-this.ppeEfficacy)
	// remove that amount from the HCW's surface and add it to the ppe surface
	_HCWMAINSURFACE.addContam(-leakage, schedule.getTickCount())
	ppeSurface.addContam(leakage, schedule.getTickCount())

	sum.ppe = true
	hcw.setProperty("surface", ppeSurface)
    }

    public scheduleTouches(VisitSummaryEvent sum) {
	int near = sum.nearTouches
	int far = sum.farTouches
	int pat = sum.patientTouches

	//evenly distribute the touches over the visit duration
	int total = near+far+pat
	double duration = sum.getDuration()
	double intraTouchTime = duration/total
	double current = schedule.getTickCount()

	//add them all to an array,
	ArrayList<String> touches = new ArrayList<String>()
	for (int i = 0; i<total; i++) {
	    if (i<near) {
		touches.add("n")
	    } else if (i < near+far){
		touches.add("f")
	    } else {
		touches.add("p")
	    }
	}
	//shuffle them,
	Collections.shuffle(touches)

	//then schedule them at even intervals over the course of the visit
	for (int i=0; i<touches.size(); i++) {
	    double next = current+(intraTouchTime*i)
	    ScheduleParameters sp = ScheduleParameters.createOneTime(next)
	    String zone = touches.get(i)
	    def next_touch = schedule.schedule(sp, this, "touch", sum.patient.room, zone, sum.patient.toString())
	}
    }

    public void touch(Room r, String zone, String touchee) {
	Surface target = null
	if (zone == "n") {
	    target = Chooser.chooseOne(r.getNearZone())
	} else if (zone=="f") {
	    target = Chooser.chooseOne(r.getFarZone())
	} else {
	    target = patient.getProperty("surface")
	}

	//this.main.event("TOUCH", this.hcw.toString() + " touching " + target.getName(), (Agent)this.hcw, (Agent)patient, "")
	Surface hcwSurface = hcw.getProperty("surface")
	transferContamination(hcwSurface, target, touchee)
    }

    public transferContamination(Surface toucher, Surface touchee, String toucheeName) {
	Parameters params = RunEnvironment.getInstance().getParameters()
	TouchSurfaceAction tsa = new TouchSurfaceAction(hcw, toucher, touchee, toucheeName)

	double surfaceLoad = touchee.getLoad(schedule.getTickCount())
	double toucherLoad = toucher.getLoad(schedule.getTickCount())
	double surfaceArea = touchee.getSurfaceArea()
	double toucherArea = toucher.getSurfaceArea()

	double effectiveArea = Math.min(surfaceArea, toucherArea)
	double effectiveSurfaceLoad = surfaceLoad * effectiveArea/surfaceArea
	double effectiveToucherLoad = toucherLoad * effectiveArea/toucherArea

	double difference = effectiveToucherLoad - effectiveSurfaceLoad
	double surfaceTransferFraction = params.getDouble("surfaceTransferFraction")

	double max_transfer = difference/2.0
	double effective_transfer = max_transfer * surfaceTransferFraction

	if (effective_transfer > 0) {
	    double tests = 0
	}

	toucher.addContam(-1*effective_transfer, schedule.getTickCount())
	touchee.addContam(1*effective_transfer, schedule.getTickCount())
	tsa.setAfterLoads(toucher, touchee)
	ContaminationTransferEvent cte = new ContaminationTransferEvent(toucher.getName(), touchee.getName(), effective_transfer)
	fireActionPerformed(cte)
	fireActionPerformed(tsa)

	main.event("CONTAM_TRANSFER", cte.toString(), null, null, "")
    }

    public leave(VisitSummaryEvent sum) {
	Room r = sum.patient.room
	//get the current hcw surface
	Surface s = sum.hcw.getProperty("surface")
	//get the load on leaving the room.
	sum.hcwLoadBeforeDoffAndHH = s.getLoad()
	doff(sum)
	sum.endHH = washHandsOnExit(sum)
	fireActionPerformed(new RoomLeave(sum.hcw, sum.patient.room, "leave"))
	sum.hcw.inCommonArea=true

	double currLoad = hcw.getProperty("surface").getLoad(schedule.getTickCount())
	double loadDelta = currLoad-sum.initHcwLoad
	sum.setTermHcwLoad(currLoad)
	sum.setTermPatientLoad(sum.patient.getProperty("surface").getLoad(schedule.getTickCount()))
	main.event("LEAVE", sum.hcw.toString()  + " (load " + currLoad +  " delta: " + loadDelta  + ") to " + sum.patient.toString() + " in " + r.toString(), (Agent)sum.hcw, (Agent)sum.patient, "")
	fireActionPerformed(sum)
	start()
    }


    public void doff(VisitSummaryEvent sum) {
	if (sum.ppe) {
	    HealthCareWorker hcw = sum.hcw
	    Surface ppe = hcw.getProperty("surface")
	    double load = ppe.getLoad(schedule.getTickCount())

	    double toAdd = load*(1-ppeEfficacy)
	    _HCWMAINSURFACE.addContam(toAdd, schedule.getTickCount())
	    hcw.setProperty("surface", _HCWMAINSURFACE)
	    _HCWMAINSURFACE = null
	}
    }

    public boolean washHandsOnExit(VisitSummaryEvent sum) {
	sum.hcw.handHygeineOpportunities += 1
	boolean washHands = hhManager.checkHandHygeine(main.vTypes.get(sum.visitType), sum.hcw, sum.patient.room, sum.patient, false)
	def ret = false;
	if (washHands) {
	    sum.hcw.handHygeieneEvents += 1
	    HandHygieneManager.Types type = hhManager.getType(sum.patient)
	    fireActionPerformed(new WashHandsEvent(sum.hcw, type))
	    ret = true
	}
	return ret
    }
}