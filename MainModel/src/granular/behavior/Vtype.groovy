package granular.behavior

import cern.jet.random.AbstractDistribution
import cern.jet.random.Gamma
import repast.simphony.random.RandomHelper

class Vtype {
    public Vtype.TYPE type
    public Gamma durationDistribution
    private HashMap<String, Double> data;
    private String typePrefix
    public AbstractDistribution near,far,independent_hh




    public enum TYPE {
	ONE,TWO,THREE,FOUR,FIVE
    }

    //were using for Gamma distribution (cern.jet vs. Apache Commons)
    //kept changing.
    private Gamma getGamma(double shape, double scale) {
	double alpha = shape;
	double lambda = 1.0/scale;
	return RandomHelper.createGamma(alpha, lambda);

    }








    public Double get(Object key) {

	String k = (String) key
	k = this.typePrefix + k
	if (!data.containsKey(k)) {
	    print("key: " + k)
	}
	return Double.parseDouble(data.get(k))


    }



    public Vtype(Vtype.TYPE t, Map<String, Double> inputs) {
	this.type = t
	this.data = inputs
	this.typePrefix = t.name() + "_";
	double dur_shape = this.get("gamma_duration_shape")
	double dur_scale = this.get("gamma_duration_scale")
	this.durationDistribution = getGamma(dur_shape, dur_scale)
	this.near = RandomHelper.createPoisson(this.get("poisson_near_touches_mean"))
	this.far = RandomHelper.createPoisson(this.get("poisson_far_touches_mean"))
	this.independent_hh = RandomHelper.createPoisson(this.get("poisson_gel_soap_touches_mean"))
    }








}
