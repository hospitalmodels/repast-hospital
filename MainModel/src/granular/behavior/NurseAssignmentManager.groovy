package granular.behavior

import agents.HealthCareWorker
import agents.Patient
import mainModel.GranularBuilder
import repast.simphony.engine.schedule.ISchedule
import util.TimeUtils

class NurseAssignmentManager {
    private GranularBuilder main
    private double shiftDuration = 0.5
    private int lastNurseAssigned = 0
    ArrayList<HealthCareWorker> nurses
    ISchedule schedule
    TimeUtils timeUtils = new TimeUtils()

    public NurseAssignmentManager(GranularBuilder main) {
	this.main = main
	this.nurses = main.nurses
	this.schedule = timeUtils.getSchedule()
    }

    private clear_all_assignments() {
	for (HealthCareWorker n : this.nurses) {
	    n.assignedPatients.clear()
	    n.getProperty("surface").clear(schedule.getTickCount())
	}
    }

    public newShift() {
	if (main.patients.size() == 0) {
	    return
	}
	clear_all_assignments()
	for (int i = 0; i<this.main.patients.size(); i++) {
	    lastNurseAssigned = i % this.nurses.size()
	    this.nurses.get(lastNurseAssigned).assignedPatients.add(main.patients.get(i))
	}
    }

    public void newPatient(Patient p) {
	lastNurseAssigned++
	lastNurseAssigned %= 10
	this.nurses.get(lastNurseAssigned).assignedPatients.add(p)
    }
}


