package granular.behavior

import actionevents.RoomLeave
import actionevents.RoomVisit
import actionevents.TouchSurfaceAction
import actionevents.VisitSummaryEvent
import actionevents.WashHandsEvent
import agents.Agent
import agents.AgentType
import agents.HealthCareWorker
import agents.Patient
import cern.jet.random.Gamma
import mainModel.GranularBuilder
import processes.Process
import repast.simphony.engine.environment.RunEnvironment
import repast.simphony.engine.schedule.ISchedulableAction
import repast.simphony.engine.schedule.ScheduleParameters
import repast.simphony.parameter.Parameters
import repast.simphony.space.graph.Network
import structure.Room
import structure.Surface
import util.Chooser
import util.InputData
import util.TimeUtils


class GranularVisit extends Process{

    private Gamma distribution
    private HealthCareWorker hcw
    private AgentType agentType
    private Network<Object> assigned
    private boolean assignedVisit
    private ISchedulableAction visitAction, leaveAction
    private ArrayList<Double> visitTypeProbs = new ArrayList<Double>()
    private GranularBuilder main
    private HandHygieneManager handHygieneMan
    private Surface _HCWMAINSURFACE  //this will hold a hcw's main surface when they're wearing ppe
    def params = RunEnvironment.getInstance().getParameters();
    public static final double HOURS = 1.0/24.0;
    public static final double MINUTES = HOURS/60.0;
    double ppeEfficacy, ppeAdherence
    boolean wearingPpe, universalPpe
    double enteringLoad
    VisitSummaryEvent visitSummary
    public HashMap<Vtype.TYPE, Vtype> visitTypes
    private static InputData inputs
    private ArrayList<Vtype> vTypeChoices
    private boolean stopped = true;






    def GranularVisit (Gamma distro
    , HealthCareWorker hcw
    , Network<Object> assigned
    , Boolean assignedVisit
    , Map<Vtype.TYPE, Vtype> visitTypes
    , GranularBuilder main
    , InputData inputs){

	super(1)
	this.distribution = distro;
	this.hcw = hcw;
	this.agentType = hcw.type
	this.assignedVisit = assignedVisit
	this.visitTypes = visitTypes
	this.main = main
	this.assigned = assigned
	this.handHygieneMan = new HandHygieneManager()
	this.ppeAdherence = params.getDouble("ppeIsolationAdherenceProb")
	this.ppeEfficacy = params.getDouble("ppeEfficacy")
	this.universalPpe = params.getBoolean("universalPpe")
	this.inputs = inputs




	def hcw_type_prefix = hcw.type.name() + "_"
	this.visitTypeProbs =  new ArrayList<Double>()
	if (!this.assigned) {
	    this.visitTypeProbs.add(inputs.get(+ "type_one_random_visit_prob"))
	    this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_two_random_visit_prob"))
	    this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_three_random_visit_prob"))
	    this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_four_random_visit_prob"))
	    this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_five_random_visit_prob"))
	} else {

	    this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_one_visit_prob"))
	    this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_two_visit_prob"))
	    this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_three_visit_prob"))
	    this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_four_visit_prob"))
	    this.visitTypeProbs.add(inputs.get(hcw_type_prefix + "type_five_visit_prob"))

	}

	this.vTypeChoices = new ArrayList<Vtype>()
	this.vTypeChoices.add(visitTypes.get(Vtype.TYPE.ONE))
	this.vTypeChoices.add(visitTypes.get(Vtype.TYPE.TWO))
	this.vTypeChoices.add(visitTypes.get(Vtype.TYPE.THREE))
	this.vTypeChoices.add(visitTypes.get(Vtype.TYPE.FOUR))
	this.vTypeChoices.add(visitTypes.get(Vtype.TYPE.FIVE))

    }



    public Object start() {
	this.stopped = false;
	double tte = distro.sample()*HOURS
	double current = schedule.getTickCount()

	ScheduleParameters schedParams = ScheduleParameters.createOneTime(tte+current)
	if (assignedVisit) {
	    visitAction = schedule.schedule(schedParams, this, "assignedRoomVisit")
	} else {
	    visitAction = schedule.schedule(schedParams, this, "visit")
	}
    }

    public Object stop() {
	if (stopped) {
	    return;
	}
	this.stopped = true;

    }

    public specialtyVisit() {
	if (this.stopped) {
	    return;
	}
	if (hcw.type == AgentType.RT) {
	    if (main.specialtyServices.needsRT.size() > 0) {
		Patient p = main.specialtyServices.getOneRTPatient()
		visitSummary = new VisitSummaryEvent(hcw, p, schedule.getTickCount())
		visitSummary.setIsRandom(true)
		do_visit(p)
	    } else {
		start()
	    }

	} else if (hcw.type == AgentType.PT) {
	    if (main.specialtyServices.needsPT.size() > 0) {
		Patient p = main.specialtyServices.getOnePTPatient()
		visitSummary = new VisitSummaryEvent(hcw, p, schedule.getTickCount())
		visitSummary.setIsRandom(true)
		do_visit(p)
	    } else {
		start()
	    }
	} else {

	    start()
	}
    }

    public visit() {
	if (this.stopped) {
	    return;
	}
	if (hcw.type == AgentType.RT || hcw.type == AgentType.PT) {
	    specialtyVisit()
	    return
	}

	if (main.patients.size() > 0 && hcw.inCommonArea==true) {
	    Agent a = Chooser.chooseOne(main.patients)
	    Patient p = (Patient)a
	    visitSummary = new VisitSummaryEvent(hcw, p, schedule.getTickCount())
	    visitSummary.setIsRandom(true)
	    do_visit(p)


	} else {
	    start()
	}
    }



    public do_visit(Patient p) {
	if (this.stopped) {
	    return;
	}

	if (p.getProperty("isolated") != null && p.getProperty("isolated")){
	    visitSummary.isolation=true
	}



	Vtype thisVisitType = Chooser.choose(vTypeChoices, visitTypeProbs)
	visitSummary.setVisitType(thisVisitType.type)

	def elapsedDuration = thisVisitType.durationDistribution.nextDouble()
	elapsedDuration = elapsedDuration / 60.0
	elapsedDuration = elapsedDuration / 24.0
	def timeOfDeparture = schedule.getTickCount() + elapsedDuration

	visitSummary.setEnd(timeOfDeparture)
	Room r = p.getRoom()

	ScheduleParameters schedulParams = ScheduleParameters.createOneTime(timeOfDeparture)
	leaveAction = schedule.schedule(schedulParams, this, "leave", p, r, hcw, thisVisitType)

	int near_touches = thisVisitType.near.nextInt()
	int far_touches = thisVisitType.far.nextInt()
	int patient_touches = thisVisitType.near.nextInt()
	int independent_hh = thisVisitType.independent_hh.nextInt()

	visitSummary.setNearTouches(near_touches)
	visitSummary.setFarTouches(far_touches)
	visitSummary.setPatientTouches(patient_touches)
	visitSummary.setIndependent_hh(independent_hh)

	String randomFlag = "VISIT"
	if (visitSummary.getIsRandom()) {
	    randomFlag = "RANDOM_VISIT"
	}

	main.event(randomFlag, hcw.toString() + " (load: " + hcw.getProperty("surface").getLoad(schedule.getTickCount()) + ")"  + " to " + p.toString() + " in " + room.toString() + " (" + thisVisitType.toString() +  " " + near_touches + "n/" + far_touches + "f)", (Agent)hcw, (Agent)a, "")
	hcw.setInCommonArea(false)
	this.enteringLoad = hcw.getProperty("surface").getLoad(schedule.getTickCount())
	visitSummary.setInitHcwLoad(enteringLoad)

	fireActionPerformed(new RoomVisit(hcw, r, p, thisVisitType, "visit") )
	r.visit(hcw, p)
	hcw.handHygeineOpportunities+=1
	if (handHygieneMan.checkHandHygeine(thisVisitType, hcw, room, p, true)){
	    visitSummary.setStartHH(true)
	    hcw.handHygeieneEvents+=1
	    HandHygieneManager.Types type = handHygieneMan.getType(p)
	    fireActionPerformed(new WashHandsEvent(hcw, type))
	}
	isolationCheck(p, r, hcw, thisVisitType)
	visitSummary.setInitPatientLoad(p.getProperty("surface").getLoad(schedule.getTickCount()))
	createTouches(near_touches, far_touches, patient_touches, elapsedDuration, r, p)
	createIndependentHH(independent_hh, elapsedDuration, p)
    }

    public createIndependentHH(int count, double time, Patient p) {
	double interval = time/(count+2)
	time += interval

	for (i=0; i<count; i++) {
	    double next = interval*i
	    ScheduleParameters schedParams = ScheduleParameters.createOneTime(next)
	    ISchedulableAction nextTouch = schedule.schedule(schedParams, this, "independentHH",p)

	}
    }

    public independentHH(Patient p) {
	HandHygieneManager.Types type = handHygieneMan.getType(p)
	fireActionPerformed(new WashHandsEvent( hcw, type))
    }

    public assignedRoomVisit() {
	if (assigned != null && assigned.getOutDegree(hcw)>0 && hcw.inCommonArea==true) {
	    Patient p = assigned.getRandomAdjacent(hcw)
	    visitSummary = new VisitSummaryEvent(hcw, p, schedule.getTickCount())
	    visitSummary.setIsRandom(false)
	    do_visit(p)

	} else {
	    start()
	}
    }



    private createTouches(int near, int far, int patient_touches, double duration, Room room, Patient patient) {
	int n = near
	int f = far
	int p = patient_touches
	int total = n+f+p
	double intratouchtime = duration/total
	double current = schedule.getTickCount()
	ArrayList<String> touches = new ArrayList<String>()
	for (int i=0; i<total; i++) {
	    if (i < near) {
		touches.add("n")
	    } else if (i < n+f){
		touches.add("f")
	    } else {
		touches.add("p")
	    }
	}
	Collections.shuffle(touches)
	for (int i = 0; i<touches.size(); i++ ) {
	    double next = current + (intratouchtime*i)
	    ScheduleParameters schedParams = ScheduleParameters.createOneTime(next)
	    String zone = touches.get(i)
	    ISchedulableAction next_touch = schedule.schedule(schedParams, this, "touch", room, zone, patient)
	}

    }

    public touch(Room room, String zone, Patient patient) {
	Surface target = null;
	if (zone == "n") {
	    target = Chooser.chooseOne(room.getNearZone())
	} else if (zone == "f"){
	    target = Chooser.chooseOne(room.getFarZone())
	} else {
	    target = patient.getProperty("surface")
	}

	if (target.getName().contains("DOCTOR") || target.getName().contains("NURSE")) {
	    for (int i = 0; i<1; i++) {

	    }
	}

	main.event("TOUCH", hcw.toString() + " touching " + target.getName(), (Agent)hcw, (Agent)patient, "")
	Surface hcwSurface = hcw.getProperty("surface")
	transferContamination(hcwSurface, target)

    }

    public transferContamination(Surface toucher, Surface touchee) {
	Parameters params = RunEnvironment.getInstance().getParameters()
	double transferFraction = params.getDouble("surfaceTransferFraction")
	TouchSurfaceAction tsa = new TouchSurfaceAction(toucher, touchee)

	double load1 = touchee.getLoad(schedule.getTickCount())
	double load2 = toucher.getLoad(schedule.getTickCount())
	double area1 = toucher.getSurfaceArea()
	double area2 = toucher.getSurfaceArea()


	double contact_area = Math.min(area1, area2)
	double touchLoad1 = load1 * contact_area / area1
	double touchLoad2 = load2 * contact_area / area2
	double difference = (touchLoad1 - touchLoad2)

	double toTransfer = difference * transferFraction

	toucher.addContam(toTransfer, schedule.getTickCount())
	touchee.addContam(-1*toTransfer, schedule.getTickCount())


	main.event("CONTAM_TRANSFER", cte.toString(), null, null, "")



    }

    public leave(Patient p, Room r, HealthCareWorker hcw, Vtype vtype) {
	r.leave(hcw)
	visitSummary.setHcwLoadBeforeDoffAndHH(hcw.getProperty("surface").getLoad())
	doff()
	hcw.handHygeineOpportunities+=1
	if (handHygieneMan.checkHandHygeine(vtype, hcw, r, p, false)){
	    visitSummary.setEndHH(true)
	    hcw.handHygeieneEvents+=1
	    HandHygieneManager.Types type = handHygieneMan.getType(p)
	    fireActionPerformed(new WashHandsEvent(hcw, type))
	}
	fireActionPerformed(new RoomLeave(hcw, r, "leave") )

	hcw.setInCommonArea(true)
	//todo: move this main call to the action performed on granularbuilder
	double currLoad = hcw.getProperty("surface").getLoad(schedule.getTickCount())
	double loadDelta = currLoad-this.enteringLoad
	visitSummary.setTermHcwLoad(currLoad)
	visitSummary.setTermPatientLoad(p.getProperty('surface').getLoad(schedule.getTickCount()))
	main.event("LEAVE", hcw.toString()  + " (load " + currLoad +  " delta: " + loadDelta  + ") to " + p.toString() + " in " + r.toString(), (Agent)hcw, (Agent)p, "")
	fireActionPerformed(visitSummary)
	start()
    }

    public isolationCheck(Patient p, Room r, HealthCareWorker hcw, Vtype vtype) {
	if (p.getProperty("isolated") || this.universalPpe)	{
	    if (Chooser.randomTrue(ppeAdherence)) {
		don()
	    }
	}
    }

    public don() {
	//println(hcw.toString() + "donning")
	//println("time: " + schedule.getTickCount())
	_HCWMAINSURFACE = hcw.getProperty("surface") //hcw hands
	//println("hcw start load = " + _HCWMAINSURFACE.getLoad(schedule.getTickCount()))
	def ppeSurface = new Surface(hcw.toString(), TimeUtils.getSchedule().getTickCount()) //gloves/gown
	double leakage = _HCWMAINSURFACE.getLoad(schedule.getTickCount()) * (1-ppeEfficacy) // amount of load from hands onto gloves
	_HCWMAINSURFACE.addContam(-leakage, schedule.getTickCount())
	ppeSurface.addContam(leakage, schedule.getTickCount())
	//println("ppeEff: " + ppeEfficacy + ", transfer=" + leakage)
	//println("hcw " + _HCWMAINSURFACE.getLoad())
	//println("ppe" + ppeSurface.getLoad(schedule.getTickCount()))
	//println()
	//println()

	visitSummary.setPpe(true)

	hcw.setProperty("surface", ppeSurface)
	this.wearingPpe = true
    }

    public doff() {
	if (this.wearingPpe) {
	    //println(hcw.toString() + "doffing")
	    Surface ppe = hcw.getProperty("surface")
	    double load = ppe.getLoad(schedule.getTickCount())
	    //println("time: " + schedule.getTickCount())
	    //println("load on main surface: " + _HCWMAINSURFACE.getLoad())
	    //println("load on ppe: " + load)
	    double toAdd = load*(1-ppeEfficacy)
	    //println("to add: " + toAdd)
	    _HCWMAINSURFACE.addContam(toAdd, schedule.getTickCount())
	    hcw.setProperty("surface",_HCWMAINSURFACE)
	    _HCWMAINSURFACE = null
	    this.wearingPpe = false

	}
    }
}
