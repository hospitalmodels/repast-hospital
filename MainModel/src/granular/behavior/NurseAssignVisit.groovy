package granular.behavior

import agents.HealthCareWorker
import agents.Patient
import cern.jet.random.Gamma
import mainModel.GranularBuilder

class NurseAssignVisit extends VisitRandom {

    public NurseAssignVisit(Gamma distro, HealthCareWorker hcw, GranularBuilder main) {
	super(distro, hcw, main)
	// TODO Auto-generated constructor stub
    }

    @Override
    public Patient choosePatient() {
	this.patientCollection = this.hcw.assignedPatients
	return super.choosePatient()
    }
}
