package granular.behavior

import agents.AgentType
import util.Chooser

class VisitTypeProbsByRole {
	
	public static final ArrayList<Double> Overall = new ArrayList<Double>(
		Arrays.asList(0.24, 0.15, 0.12, 0.15, 0.34)
		)
	
	public static final ArrayList<Double> AssignedNurseAssignedRoom = new ArrayList<Double>(
			Arrays.asList(0.18, 0.12, 0.1, 0.15, 0.45)
			)
	
	public static final ArrayList<Double> AssignedNurseRandom = new ArrayList<Double>(
			Arrays.asList(0.47, 0.21, 0.09, 0.1, 0.13)
		)
		
	public static final ArrayList<Double> Floater = new ArrayList<Double>(
			Arrays.asList(0.47, 0.21, 0.09, 0.1, 0.13)
		)
	
	public static final ArrayList<Double> Doctor = new ArrayList<Double>(
		Arrays.asList(0.51, 0.23, 0.09, 0.08, 0.09)
		)
	
	public static final ArrayList<Double> Cna = new ArrayList<Double>(
		Arrays.asList(0.27, 0.13, 0.11, 0.18, 0.31)
		)
	
	public static final ArrayList<Double> Pts = new ArrayList<Double>(
		Arrays.asList(0.17, 0.17, 0.19, 0.24, 0.23)
		)
		
	public static final ArrayList<Double> Rts = new ArrayList<Double>(
		Arrays.asList(0.13, 0.25, 0.28, 0.18, 0.16)
		)
		
	

	public static final HashMap<String, ArrayList<Double>> PROBS = ['NURSE': VisitTypeProbsByRole.AssignedNurseAssignedRoom,
			'NURSE_RANDOM': VisitTypeProbsByRole.AssignedNurseRandom,
			'NURSE_FLOAT': VisitTypeProbsByRole.Floater,
			'DOCTOR': VisitTypeProbsByRole.Doctor,
			'CNA': VisitTypeProbsByRole.Cna,
			'PT': VisitTypeProbsByRole.Pts,
			'RT': VisitTypeProbsByRole.Rts]
}
