package granular.behavior

import java.awt.event.ActionEvent
import java.awt.event.ActionListener

import actionevents.PatientAdmitted
import actionevents.SurveillanceEvent
import agents.Patient
import processes.Process
import repast.simphony.engine.environment.RunEnvironment
import repast.simphony.parameter.Parameters
import util.Chooser

class Surveillance extends Process implements ActionListener{

    double sensitivity
    double specificity
    boolean surveillance
    Parameters params


    public Surveillance(double intra) {
	super(intra);
	this.params = RunEnvironment.getInstance().getParameters()
	this.surveillance = params.getBoolean("surveillance")
	this.sensitivity = params.getDouble("surveillanceSensitivity")
	this.specificity = params.getDouble("surveillanceSpecificity")
    }

    @Override
    public void actionPerformed(ActionEvent a) {
	if (a instanceof PatientAdmitted && this.surveillance) {
	    PatientAdmitted pa = (PatientAdmitted) a
	    surveille(pa.getPatient())
	}
    }

    private void surveille(Patient p) {
	String result = ""
	if (p.getProperty("imported") == null) {
	    //non colonized import
	    if (Chooser.randomTrue(1.0 - this.specificity)) {
		p.setProperty("isolated", true)
		result = "false_positive"
	    } else {
		result = "true_negative"
	    }
	} else if (p.getProperty("imported") == true) {
	    if (Chooser.randomTrue(this.sensitivity)) {
		result = "true_positive"
		p.setProperty("isolated", true)
	    } else {
		result = "false_negative"
	    }

	}


	SurveillanceEvent se = new SurveillanceEvent(p , result)
	fireActionPerformed(se)
    }

    @Override
    public Object start() {
	return null;
    }

    @Override
    public Object stop() {
	return null;
    }
}
