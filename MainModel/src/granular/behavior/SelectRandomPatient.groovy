package granular.behavior
import agents.Patient
import util.Chooser

class SelectRandomPatient {


    public static Patient selectRandomPatient(List<Patient> patients) {
	return Chooser.chooseOne(patients)
    }
}
