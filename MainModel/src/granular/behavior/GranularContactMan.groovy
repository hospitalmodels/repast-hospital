package granular.behavior

import agents.HealthCareWorker
import mainModel.GranularBuilder
import processes.ContactNetworkManager
import repast.simphony.space.graph.Network
import structure.Ward

class GranularContactMan extends ContactNetworkManager {
	
	public GranularContactMan(double intra, ArrayList<HealthCareWorker> hcws, Ward w, GranularBuilder main, Network<Object> assigned) {
		super(intra, hcws, w, main, assigned)
	}
	
	
	@Override
	public void assignEdges() {
		super.assignEdges()
		for (HealthCareWorker h in hcws) {
			h.getProperty("surface").clear()
		}
	}
	
}
