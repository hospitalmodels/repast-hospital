package granular.disease

import java.awt.event.ActionEvent
import java.awt.event.ActionListener

import actionevents.ColonizedEvent
import actionevents.ContaminationPulseEvent
import actionevents.DetectionEvent
import actionevents.DiseaseProgression
import actionevents.ImportationEvent
import actionevents.PatientAdmitted
import actionevents.RoomCleaningEvent
import actionevents.WashHandsEvent
import agents.AgentType
import agents.HealthCareWorker
import agents.Patient
import granular.behavior.HandHygieneManager
import mainModel.GranularBuilder
import processes.Process
import repast.simphony.engine.environment.RunEnvironment
import repast.simphony.engine.schedule.ScheduleParameters
import repast.simphony.parameter.Parameters
import repast.simphony.random.RandomHelper
import structure.Room
import structure.Surface
import util.Chooser
import util.TimeUtils



class DiseaseManager extends Process implements ActionListener{

    ArrayList<Disease.State> importStates = [
	Disease.State.SUSCEPTIBLE,
	Disease.State.COLONIZED,
	Disease.State.INFECTED,
	Disease.State.RECOVERED
    ]
    ArrayList<Double> importProbabilities = [0.9175, 0.075, 0.0075, 0.0]
    public double tuningParam

    //ArrayList<Double> importProbabilities = [1.0, 0.0, 0.0, 0.0]

    int importationCount = 0
    GranularBuilder main
    double susceptibilityOffset = 0.00001
    ArrayList<Double> dailyColonizationPrev, dailyInfectionPrev, dailyRecoveredPrev

    double percentImportersProgress = 0.04
    double percentAcquirersProgress = 0.41

    //mike list
    int totalAdmissions, totalColonizedAdmissions, totalInfectedAdmissions
    int totalAcquisitions, totalAcuirersWhoProgress, totalProgressions, detectionsAfterAtRisk, detectionsAfterAtRiskNoImporters
    double patientDays, patientDaysAtRisk
    double meanLos, meanLOSInfected, sxdaysuntreated
    int patientsIsolated, testsOrdered, casesDetected
    ArrayList<Double> los, losInfected

    public static final double DAYS_AT_RISK_THRESHOLD = 3.0

    double gelEfficacy, soapEfficacy

    private ArrayList<Integer> preIcuStayDays;
    private ArrayList<Double> preIcuStayDaysWeights;

    public int dailyIncidence = 0;





    public DiseaseManager(GranularBuilder main) {
	super(0)
	this.main = main

	this.tuningParam = RunEnvironment.getInstance().getParameters().getDouble("susceptibility")
	this.dailyColonizationPrev = new ArrayList<Double>()
	this.dailyInfectionPrev = new ArrayList<Double>()
	this.dailyRecoveredPrev = new ArrayList<Double>()

	ScheduleParameters schedParams = ScheduleParameters.createRepeating(1, 15*TimeUtils.MINUTE)
	TimeUtils.getSchedule().schedule(schedParams, this, "patientSelfColonizeCheck")

	ScheduleParameters schedParams2 = ScheduleParameters.createRepeating(1,1)
	TimeUtils.getSchedule().schedule(schedParams, this, "dailyData")

	los = new ArrayList<Double>()
	losInfected = new ArrayList<Double>()

	this.preIcuStayDays = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
	this.preIcuStayDaysWeights =
		new ArrayList<Double>(Arrays.asList(0.39, 0.18, 0.1, 0.07, 0.05, 0.04, 0.03, 0.14));
    }

    public dailyData() {
	int occupancy = main.patients.size()
	double colonized = (double)getColonizationCount()/occupancy
	double infected = (double)getInfectionCount()/occupancy
	double recovered = (double)getRecoveredCount()/occupancy
	dailyColonizationPrev.add(colonized)
	dailyInfectionPrev.add(infected)
	dailyRecoveredPrev.add(recovered)
    }



    public cleanDisease(Room r, String type) {
	double cleaningEfficacy = RunEnvironment.getInstance().getParameters().getDouble("roomCleaningEfficacy")
	if (type == "TERMINAL") {
	    cleaningEfficacy = RunEnvironment.getInstance().getParameters().getDouble("roomCleaningEfficacy")
	}
	if (type == "POSTVISIT") {
	    cleaningEfficacy = 0.30;



	    for (Surface s in r.getNearZone() + r.getFarZone()) {
		double currentLoad = s.getLoad(main.schedule.getTickCount())
		double reduction = currentLoad*cleaningEfficacy
		s.addContam(-reduction, main.schedule.getTickCount())
	    }
	    if (r.getPatients().size() > 0) {
		Patient p = r.getPatients()[0]
		double currentLoad = p.getProperty("surface").getLoad(main.schedule.getTickCount())
		double reduction = currentLoad*cleaningEfficacy
		p.getProperty("surface").addContam(-reduction, main.schedule.getTickCount())
	    }
	}
    }




    public patientSelfColonizeCheck() {
	for (Patient p  in main.patients) {
	    double load =  (double)p.getProperty("surface").getLoad(TimeUtils.getSchedule().getTickCount())
	    if (p.disease == null && load > 0) {
		Parameters params = RunEnvironment.getInstance().getParameters()
		double daysInHospital = TimeUtils.getSchedule().getTickCount() - p.admitted
		double daysInHospitalEffect = 10.0/daysInHospital
		double c = this.tuningParam*susceptibilityOffset*daysInHospitalEffect
		double probabilityOfColonization = 1-Math.exp(-1*c*load)
		//println(probabilityOfColonization)
		try {
		    if (Chooser.randomTrue(probabilityOfColonization)) {
			//println("self-colonized w/ load " + load)
			selfColonize(p, load)
		    }
		}
		catch (Exception e) {
		    println("probability of self colonize NaN")
		}
	    }
	}
    }

    public selfColonize(Patient p, double load) {
	if (p.getProperty("disease") == null) {
	    Disease disease = new Disease(p, Disease.State.SUSCEPTIBLE,false, main)
	    p.setProperty("disease", disease)
	    disease.addActionListener(this)
	    disease.colonize(load)
	    totalAcquisitions ++
	}
    }




    def importOn(Patient p, Disease.State importState) {
	Disease disease = new Disease(p, importState, true, main)
	p.setProperty("disease", disease)
	disease.addActionListener(this)
	p.setProperty("imported", true)
	if (importState == Disease.State.COLONIZED) {
	    totalColonizedAdmissions++
	} else if (importState == Disease.State.INFECTED) {
	    totalInfectedAdmissions++
	    //println("infected Admissions = " + totalInfectedAdmissions)
	}
    }

    def washHands(HealthCareWorker hcw, HandHygieneManager.Types type) {
	double load = hcw.getProperty("surface").getLoad(main.schedule.getTickCount())
	//println(load)
	double efficacy = 0
	if (type == HandHygieneManager.Types.GEL) {
	    efficacy = 0.2
	} else if (type == HandHygieneManager.Types.SOAP) {
	    efficacy = 0.9
	}

	double reduction = load * efficacy
	hcw.getProperty("surface").addContam(-1*reduction, main.schedule.getTickCount())
	//println(hcw.getProperty("surface").getLoad(schedule.getTickCount()))
	//println("-----")
    }


    @Override
    public void actionPerformed(ActionEvent e) {
	if (e instanceof PatientAdmitted) {
	    PatientAdmitted pa = (PatientAdmitted) e
	    Patient p = pa.getPatient()
	    int preIcuStayDays = getPreIcuStayDays()
	    double currentTime = TimeUtils.getSchedule().getTickCount()
	    double adjustedAdmit = currentTime-preIcuStayDays
	    if (adjustedAdmit < 0) {
		adjustedAdmit = 0
	    }
	    p.setProperty("preIcuStayDays", preIcuStayDays)
	    p.setProperty("ADMIT_TIME", adjustedAdmit )
	    totalAdmissions++
	    Disease.State importState = Chooser.choose(importStates, importProbabilities)
	    if (importState in [
			Disease.State.COLONIZED,
			Disease.State.INFECTED
		    ]) {
		//println(p.toString()+ ": " + importState)
		//TODO: SEt this back importOn(p,importState)
		importOn(p,importState)
		fireActionPerformed(new ImportationEvent(p))
	    } else {
		//println("not colonized")
	    }
	}

	if (e instanceof ColonizedEvent) {
	    fireActionPerformed(e)
	}

	if (e instanceof DiseaseProgression) {
	    Patient p = (Patient)e.getPatient()
	    if (p.discharged > 0) {
		return
	    }
	    //fireActionPerformed(e)
	    dailyIncidence+=1
	    DiseaseProgression dp = (DiseaseProgression)e
	    Disease d = (Disease)dp.getPatient().getProperty("disease")
	    d.timeOfProgression = TimeUtils.getSchedule().getTickCount()
	    totalProgressions ++
	    if (d.timeOfColonization > 0) {
		totalAcuirersWhoProgress++
	    }
	}

	if (e instanceof ContaminationPulseEvent) {
	    fireActionPerformed(e)
	}

	if (e instanceof RoomCleaningEvent) {
	    RoomCleaningEvent rce = (RoomCleaningEvent)e
	    cleanDisease(rce.room, rce.type)
	}

	if (e instanceof WashHandsEvent) {
	    WashHandsEvent whe = (WashHandsEvent)e
	    washHands(whe.getHcw(), whe.hhType)
	}

	if (e instanceof DetectionEvent) {
	    DetectionEvent de = (DetectionEvent)e
	    if (de.getPatient().getDischarged() > 0) {
		return;
	    }
	    testsOrdered ++
	    casesDetected ++
	    double progTime = de.getPatient().getProperty("disease").timeOfProgression
	    double admitTime = de.getPatient().getProperty("ADMIT_TIME")
	    double atRiskTime = admitTime+DAYS_AT_RISK_THRESHOLD
	    if (progTime > atRiskTime) {
		detectionsAfterAtRisk++
	    }
	    if (progTime > atRiskTime && de.getPatient().getProperty("disease").colonizationTime > 0) {
		detectionsAfterAtRiskNoImporters++
	    }
	    main.event("DETECTION", de.patient, de.patient, "")
	}
    }



    @Override
    public Object start() {
	return null;
    }

    @Override
    public Object stop() {
	return null;
    }



    //data reportion

    public discharge(Patient p) {
	double admittime = p.getProperty("ADMIT_TIME")
	double dischargetime = TimeUtils.getSchedule().getTickCount()
	double gross_los = dischargetime-admittime
	double los_at_risk = Math.max(gross_los-DAYS_AT_RISK_THRESHOLD, 0)
	patientDays += gross_los
	los.add(gross_los)
	if (p.getProperty("disease") && p.getProperty("disease").timeOfProgression > 0) {
	    losInfected.add(gross_los)
	    double timeAfterInfection = dischargetime-p.getProperty("disease").timeOfProgression
	    los_at_risk -= timeAfterInfection
	}
	if (p.getProperty("isolated")) {
	    patientsIsolated++
	}
	patientDaysAtRisk += los_at_risk
    }


    public int getDailyIncidence() {
	int ret = this.dailyIncidence
	this.dailyIncidence = 0
	return ret
    }
    public double getSusceptibility() {
	return RunEnvironment.getInstance().getParameters().getDouble("susceptibility")
    }

    public double getMeanDailyColonizationPrev() {
	return dailyColonizationPrev.sum()/(double)dailyColonizationPrev.size()
    }

    public double getMeanDailyInfectionPrev() {
	return dailyInfectionPrev.sum()/(double)dailyInfectionPrev.size()
    }

    public double getMeanDailyRecoveredPrev() {
	return dailyRecoveredPrev.sum()/(double)dailyRecoveredPrev.size()
    }

    public int getTotalAdmissions() {
	return totalAdmissions
    }

    public int getTotalColonizedAdmissions() {
	return totalColonizedAdmissions
    }

    public int getTotalInfectedAdmissions() {
	return totalInfectedAdmissions
    }

    public int getTotalAcquisitions() {
	return totalAcquisitions
    }

    public double getAcquisitionRate() {
	return totalAcquisitions*10000/patientDays
    }

    public int getTotalAcquirersWhoProgress() {
	return totalAcuirersWhoProgress
    }

    public int getTotalProgressions() {
	return totalProgressions
    }

    public int getDetectionsAfterAtRisk() {
	return detectionsAfterAtRisk
    }

    public double getPatientDays() {
	return patientDays
    }

    public double getPatientDaysAtRisk(){
	return patientDaysAtRisk
    }

    private getMean(ArrayList<Double> l) {
	double sum = 0.0
	for (Double n in l) {
	    sum += n
	}
	return sum / l.size()
    }

    public double getMeanLos() {
	return getMean(los)
    }

    public double getMeanLosInfected() {
	return getMean(meanLOSInfected)
    }

    public int getPatientsIsolated() {
	return patientsIsolated
    }

    public int getTestsOrdered() {
	return getPatientsIsolated()
    }

    public int casesDetected() {
	return getPatientsIsolated()
    }

    public int isUniversalPpe() {
	if (RunEnvironment.getInstance().getParameters().getBoolean("universalPpe")) {
	    return 1
	}
	return 0
    }


    public int getColonizationCount() {
	int i = 0
	for (Patient p : main.patients) {
	    if (p.getProperty("disease")) {
		Disease d = (Disease)p.getProperty("disease")
		if (d.state == Disease.State.COLONIZED) {
		    i ++
		}
	    }
	}
	return i
    }


    public int getInfectionCount() {
	int i = 0
	for (Patient p : main.patients) {
	    if (p.getProperty("disease")) {
		Disease d = (Disease)p.getProperty("disease")
		if (d.state == Disease.State.INFECTED) {
		    i ++
		}
	    }
	}
	return i
    }

    public int getIsolatedCount() {
	int i = 0
	for (Patient p: main.patients) {
	    if (p.getProperty("isolated")) {
		i++
	    }
	}
	return i
    }

    public int getRecoveredCount() {
	int i = 0
	for (Patient p : main.patients) {
	    if (p.getProperty("disease")) {
		Disease d = (Disease)p.getProperty("disease")
		if (d.state == Disease.State.RECOVERED) {
		    i ++
		}
	    }
	}
	return i
    }

    public double getInf10kpd() {
	return (double)totalProgressions*10000/getPatientDays()
    }

    public double getINf10kpdar() {
	return (double)detectionsAfterAtRisk*10000/getPatientDaysAtRisk()
    }

    public double getHandHygieneIntervention() {
	return RunEnvironment.getInstance().getParameters().getDouble("handHygieneIntervention")
    }

    public double getHHOverallAdherence() {
	ArrayList<HealthCareWorker>	all = new ArrayList<HealthCareWorker>()
	all.addAll(main.nurses)
	all.addAll(main.cnas)
	all.addAll(main.doctors)
	all.addAll(main.pts)
	all.addAll(main.rts)
	int opportunities = 0
	int events = 0
	for (HealthCareWorker hcw : all) {
	    opportunities += hcw.handHygeineOpportunities
	    events += hcw.handHygeieneEvents
	}

	return (double)events/(double)opportunities
    }

    public double getHHAdherenceByType(AgentType t) {
	ArrayList<HealthCareWorker>	all = new ArrayList<HealthCareWorker>()
	all.addAll(main.nurses)
	all.addAll(main.cnas)
	all.addAll(main.doctors)
	all.addAll(main.pts)
	all.addAll(main.rts)
	int opportunities = 0
	int events = 0
	for (HealthCareWorker hcw : all) {
	    if (hcw.getType() == t) {
		opportunities += hcw.handHygeineOpportunities
		events += hcw.handHygeieneEvents
	    }
	}
	return (double)events/(double)opportunities
    }

    public double getHHNurseAdherence() {
	return getHHAdherenceByType(AgentType.NURSE)
    }


    public double getHHCnaAdherence() {
	return getHHAdherenceByType(AgentType.CNA)
    }

    public double getHHDoctorAdherence() {
	return getHHAdherenceByType(AgentType.DOCTOR)
    }

    public double getHHPtAdherence() {
	return getHHAdherenceByType(AgentType.PT)
    }

    public double getHHRtAdherence() {
	return getHHAdherenceByType(AgentType.RT)
    }

    public double getPpeEfficacy() {
	return RunEnvironment.getInstance().getParameters().getDouble("ppeEfficacy")
    }

    public double getPpeAdherence() {
	return RunEnvironment.getInstance().getParameters().getDouble("ppeIsolationAdherenceProb")
    }

    public double getScene() {
	return RunEnvironment.getInstance().getParameters().getInteger("scenario")
    }

    public double get_All_cause_isolation() {
	return RunEnvironment.getInstance().getParameters().getDouble("all_cause_isolation")
    }
    public double getSurveillanceSensitivity() {
	return RunEnvironment.getInstance().getParameters().getDouble("surveillanceSensitivity")
    }
    public double getSurveillanceSpecificity() {
	return RunEnvironment.getInstance().getParameters().getDouble("surveillanceSpecificity")
    }

    public int getPreIcuStayDays() {
	double test = RandomHelper.nextDouble();
	if (test <= 1.7 / 6.0) {
	    return 0;
	}

	test = RandomHelper.nextDouble();
	//System.out.println("test" + test)
	int i = 0;
	double cumulative = 0.0
	for (double d : this.preIcuStayDaysWeights) {
	    //System.out.println(d)
	    cumulative += this.preIcuStayDaysWeights.get(i)
	    if (test < cumulative) {
		return this.preIcuStayDays.get(i);
	    }
	    i++;
	}
	return 10;
    }
    public double getTotalPatientLoad() {
	double ret = 0.0
	for (Patient p : main.patients) {
	    ret += ((Surface)p.getProperty("surface")).getLoad()
	}
	return ret
    }

    public int getNumberOfRoomsWithDetectableContam() {
	int roomCount = 0
	for (Room r in main.rooms) {
	    ArrayList<Surface> surfs = new ArrayList<Surface>()
	    surfs.addAll(r.getNearZone())
	    surfs.addAll(r.getFarZone())
	    for (Surface s in surfs) {
		if (s.getLoad() > 10000) {
		    roomCount++
		}
	    }
	}
	return roomCount
    }

    public double getDecayRate() {
	return RunEnvironment.getInstance().getParameters().getDouble("decayRate")
    }

    public double getSurfaceTransferFraction() {
	return RunEnvironment.getInstance().getParameters().getDouble("surfaceTransferFraction")
    }
}