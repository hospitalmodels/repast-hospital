package granular.disease

import static granular.disease.Disease.State.RECOVERED

import actionevents.ColonizedEvent
import actionevents.ContaminationPulseEvent
import actionevents.DetectionEvent
import actionevents.DiseaseProgression
import agents.HealthCareWorker
import agents.Patient
import cern.jet.random.AbstractDistribution
import processes.Process
import repast.simphony.engine.environment.RunEnvironment
import repast.simphony.engine.schedule.DefaultAction
import repast.simphony.engine.schedule.ScheduleParameters
import repast.simphony.random.RandomHelper
import structure.Dischargeable
import structure.Room
import structure.Surface
import util.Chooser
import util.TimeUtils

class Disease extends Process {

    enum State {
	SUSCEPTIBLE, COLONIZED, INFECTED, RECOVERED
    }


    enum PulseValues{
	PATIENT(21000),
	NEAR(17000),
	FAR(4200)

	//PATIENT(0),
	//NEAR(0),
	//FAR(0)
	int amount

	private PulseValues(int a) {
	    this.amount = a
	}
    }

    public Dischargeable main
    public State state
    public Patient patient
    public boolean imported, nonProgressingImports, discharge
    public double timeOfColonization, timeOfProgression, timeOfDetection, timeOfRecovery
    public double tuningParam

    public static final AbstractDistribution PROGRESS_DISTRO = RandomHelper.createNormal(3.0, 0.5)
    // Mike gave me this distribution. -^
    //public static final AbstractDistribution PROGRESS_DISTRO = RandomHelper.createNormal(0.2, 0.1)

    public static final double pulseFrequency = 2.0*TimeUtils.HOUR

    public Disease(Patient p, State s, boolean i, Dischargeable target) {
	super(0)
	this.patient = p
	this.state = s
	this.imported = false
	this.discharge = false
	this.main = target



	if (state == State.COLONIZED) {
	    if (true && Chooser.randomTrue(0.04)) {
		startProgressionTimer()
	    }

	    startContaminationPulse()
	    timeOfColonization = -1
	}

	if (state == State.INFECTED) {
	    //println("start contamination pulse")
	    startContaminationPulse()
	    double current = TimeUtils.getSchedule().getTickCount()
	    this.detection()
	    timeOfColonization = -1
	    timeOfProgression = -1
	}
    }

    public colonize(double load) {
	if (state == State.SUSCEPTIBLE) {
	    state = State.COLONIZED
	    if (Chooser.randomTrue(0.42)) {
		startProgressionTimer()
	    }
	    //println(patient.toString() + " colonized " + TimeUtils.getSchedule().getTickCount())
	    startContaminationPulse()
	    timeOfColonization = TimeUtils.getSchedule().getTickCount()
	    fireActionPerformed(new ColonizedEvent(patient, load))
	}
    }



    public startContaminationPulse() {
	if (discharge == true) {
	    return
	}
	double nextTime = TimeUtils.getSchedule().getTickCount() + pulseFrequency
	ScheduleParameters schedParam = ScheduleParameters.createRepeating(TimeUtils.getSchedule().getTickCount()+pulseFrequency, pulseFrequency)
	patient.getBehaviors().put("contamination_pulse", (Object)TimeUtils.getSchedule().schedule(schedParam, this, "pulse"))
    }

    public pulse() {
	if (discharge == true) {
	    return
	}
	if (state in [
		    State.COLONIZED,
		    State.INFECTED
		]) {
	    Room r = patient.getRoom()
	    ArrayList<Surface> near = r.getNearZone()
	    ArrayList<Surface> far = r.getFarZone()
	    Surface patSurf = patient.getProperty("surface")
	    List<HealthCareWorker> visitors = r.hcwVisitors
	    for (HealthCareWorker hcw in visitors) {
		near.add(hcw.getProperty("surface"))
	    }

	    double multiplier = 0
	    if (state == State.COLONIZED) {
		multiplier = 0.1
	    } else if (state==State.INFECTED) {
		multiplier = 1
	    }

	    double treatmentMultiplier = 1
	    if (patient.hasProperty("treatment")) {
		double timeStart = patient.getProperty("treatmentstart")
		double treatmentDuration = patient.getProperty("treatmentduration")
		double treatmentSoFar = TimeUtils.getSchedule().getTickCount()-timeStart
		treatmentMultiplier = Math.max(0, treatmentSoFar/treatmentduration)
	    }

	    patSurf.addContam(PulseValues.PATIENT.amount*multiplier*treatmentMultiplier,  TimeUtils.getSchedule().getTickCount())
	    int numNearOjbects = near.size()

	    double nearTotalSurface = 0.0
	    double farTotalSurface = 0.0
	    for (Surface s : near) {
		nearTotalSurface += s.surfaceArea
	    }
	    for (Surface s : far) {
		farTotalSurface += s.surfaceArea
	    }

	    for (Surface s : near) {
		s.addContam(PulseValues.NEAR.amount*multiplier*(s.surfaceArea/nearTotalSurface)*treatmentMultiplier,  TimeUtils.getSchedule().getTickCount())
	    }
	    int numFarObjects = far.size()
	    for (Surface s : far) {
		s.addContam(PulseValues.FAR.amount*multiplier*(s.surfaceArea/farTotalSurface)*treatmentMultiplier,  TimeUtils.getSchedule().getTickCount())
	    }




	    fireActionPerformed(new ContaminationPulseEvent(patient))

	    for (HealthCareWorker hcw in visitors) {
		near.remove(hcw.getProperty("surface"))
	    }
	}
    }

    public startProgressionTimer() {
	if (state == State.COLONIZED && discharge == false) {
	    double timeToProgression = PROGRESS_DISTRO.nextDouble()
	    double timeOfProgression = TimeUtils.getSchedule().getTickCount() + timeToProgression
	    ScheduleParameters schedParams = ScheduleParameters.createOneTime(timeOfProgression)
	    TimeUtils.getSchedule().schedule(schedParams, this, "progression")
	}
    }


    public void progression() {
	if (patient.discharged > 0) {
	    return
	}

	if (this.state == State.COLONIZED) {
	    this.state = State.INFECTED
	    patient.fireActionPerformed(new DiseaseProgression(patient))
	    double current = TimeUtils.getSchedule().getTickCount()
	    double timeToDetect = current + RandomHelper.createUniform(0.5, 1.0).nextDouble()
	    ScheduleParameters schedParams = ScheduleParameters.createOneTime(timeToDetect)
	    TimeUtils.getSchedule().schedule(schedParams, this, "detection")
	    timeOfProgression = TimeUtils.getSchedule().getTickCount()

	    DefaultAction dischargeEvent = patient.getBehaviors().remove("discharge")
	    double originaldc = dischargeEvent.nextTime
	    //println("Original dc: " + originaldc)
	    double newdc = originaldc + 2.6
	    dischargeEvent.nextTime = newdc

	    //wrr:  For future reference, the schedule will pick up the change
	    //in the "nextTime" value of a scheduleable in the queue.
	    //TimeUtils.getSchedule().removeAction(dischargeEvent)
	    //patient.setProperty("delayDischarge", true)

	    //schedParams = ScheduleParameters.createOneTime(newdc)
	    //TimeUtils.getSchedule().schedule(schedParams, main, "discharge", patient)
	}
    }




    public void detection() {

	if (patient.discharged > 0) {
	    return
	}
	patient.setProperty("detected", true)
	if (RunEnvironment.getInstance().getParameters().getInteger("isolateOnSuspicion")==1) {
	    patient.setProperty("isolated", true)
	}
	double current = TimeUtils.getSchedule().getTickCount()
	double timeToDetect = current + 6*24 //hours
	ScheduleParameters schedParams = ScheduleParameters.createOneTime(timeToDetect)
	TimeUtils.getSchedule().schedule(schedParams, this, "treatment")
	fireActionPerformed(new DetectionEvent(patient))
    }

    public void treatment() {

	patient.setProperty("isolated", true) //if not already
	patient.setProperty("treatment", true)
	patient.setProperty("treatmentstart", TimeUtils.getSchedule().getTickCount())
	double treatmentDuration = 10
	patient.setProperty("treatmentduration", treatmentDuration)
	double treatmentStopsAt = TimeUtils.getSchedule().getTickCount()
	patient.setProperty("treatmentStop", treatmentStopsAt)
	ScheduleParameters schedParams = ScheduleParameters.createOneTime(treatmentStopsAt)
	TimeUtils.getSchedule().schedule(schedParams, this, "treatmentStop")
    }

    public treatmentStop() {
	if (discharge == true) {
	    return
	}
	patient.setProperty("treatment", false)
	state = RECOVERED
    }



    @Override
    public Object start() {
    }

    @Override
    public Object stop() {
	discharge = true
	patient.setProperty("surface", new Surface(p.toString(), 10, 300));
	state = RECOVERED
    }
}
