package metacomponents

import agents.HealthCareWorker
import agents.Patient
import structure.Room
import structure.Surface
import agents.Agent
import agents.AgentType

class GranularMetaConfiguration {
	
	static configure() {
		agent()
		addHandHygieneAdherencesToHcws()
		room()
		
	}
	
	static agent(){
		Agent.metaClass.hhAdherences 
	}
	
	static addHandHygieneAdherencesToHcws() {
		
	}
	
	static room() {
		Room.metaClass.isolation = false
	}
	
	
	
	
	
}