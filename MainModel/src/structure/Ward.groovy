package structure

import java.util.ArrayList

import agents.Nurse;
import agents.Patient
import repast.simphony.context.DefaultContext;

class Ward extends DefaultContext<Object>{
	ArrayList<Room> rooms
	ArrayList<Bed> beds
	ArrayList<Object> nurses
	Facility hospital
	int capacity


	Ward(Facility hospital){
		rooms = new ArrayList<Room>()
		nurses = new ArrayList<Nurse>()
		this.hospital=hospital
	}
	
	Ward(){
		rooms = new ArrayList<Room>()
		nurses = new ArrayList<Nurse>()
	}
	
	int getVacancy(){
		int vac
		for (Room r: rooms){
			vac+=r.getVacancy()
		}
		return vac
	}
	
	ArrayList<Patient> getPatients(){
		ArrayList<Patient> ret = new ArrayList<Patient>()
		for (Room r : rooms){
			ret.addAll(r.getPatients())
		}
		return ret
	}
	
	def addRoom(Room room){
		rooms.add(room)
	}

	
}
