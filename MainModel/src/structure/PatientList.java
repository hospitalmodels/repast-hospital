package structure;

import java.util.ArrayList;

import agents.Agent;
import agents.Patient;

public class PatientList implements Admittable, Dischargeable {

  private ArrayList<Patient> patients;

  public PatientList() {
    this.patients = new ArrayList<Patient>();
  }

  @Override
  public void admit(Patient p) {
    this.patients.add(p);
  }

  @Override
  public void discharge(Agent a) {
    Patient p = (Patient) a;
    this.patients.remove(p);
  }

  public boolean contains(Patient p) {
    return this.patients.contains(p);
  }

  @Override
  public void admit() { // fulfil admittable inferface.
  }
}
