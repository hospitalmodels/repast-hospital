package structure;

import agents.Patient;

public interface Admittable {

  public void admit();

  public void admit(Patient p);
}
