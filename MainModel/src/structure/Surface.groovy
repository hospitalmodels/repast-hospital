package structure

import repast.simphony.engine.environment.RunEnvironment

class Surface {

    String name
    double surfaceArea
    double lastUpdate
    double load
    public double lambda = 1



    public Surface(String name, double surf=1, double time){
	this.name=name
	this.surfaceArea=surf
	double load = 0
	double lastUpdate = time

	lambda=RunEnvironment.getInstance().getParameters().getDouble("decayRate")
    }



    private void update(double t) {
	if (t > lastUpdate) {
	    double elapsed = t-lastUpdate
	    double newLoad = load*Math.exp(-1*lambda*elapsed)
	    load = newLoad
	    lastUpdate = t
	}
    }

    public double getLoad(double t) {
	update(t)
	return load
    }

    public addContam(double amount, double t) {
	update(t)
	load += amount
	load = Math.min(load, 42000)
    }

    public clear(double t) {
	load = 0
	lastUpdate = t
    }

    public String toString() {
	return name
    }
}
