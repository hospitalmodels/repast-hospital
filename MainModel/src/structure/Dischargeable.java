package structure;

import agents.Agent;

public interface Dischargeable {

	public void discharge(Agent a);
	
}
