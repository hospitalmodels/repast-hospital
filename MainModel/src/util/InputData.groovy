package util

class InputData {
	
	Map<String, Double> data=[:]
	
	def InputData(String filePath) {
		new File(filePath).eachLine { line ->
			if (line.contains("#") == false && line != "") {
				String[] keyval = line.split(':')
				data[keyval[0].trim()]=keyval[1].trim()
			}
		}
	}
	
	double get(String key) {
		try {
			return Double.parseDouble(data[key])
		} catch (Exception e) {
			println(key)
			println(e.stackTrace)
		}
	}	
}	