package granulartests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import util.Chooser;

public class ChooserTests {

	@Test
	public void coinFliptest() {
		boolean test = Chooser.coinFlip();
		assert (test || !test);
	}
	
	@Test
	public void randomTrueTest() {
		assert Chooser.randomTrue(0.0) == false;
		assert Chooser.randomTrue(1.0) == true;
		boolean check = Chooser.randomTrue(0.5);
		assert (check || !check);
	}

}
