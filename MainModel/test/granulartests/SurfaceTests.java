package granulartests;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import structure.Surface;

public class SurfaceTests {
	private Surface surface;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.surface = new Surface("surface", 0);
		
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void starting() {
		double load = (double) surface.getLoad(0);
		assertEquals(0.0, load, 0.0);
	}
	
	@Test
	public void adding() {
		surface.addContam(1.0, 1.0);
		double load = (double) surface.getLoad(1.0);
		assertEquals(1.0, load, 0.0 );
	}
	
	@Test
	public void decay() {
		double load = (double) surface.getLoad(1.5);
		assertTrue(load < 1.0);
	}
	
	
	@Test
	public void clearing() {
		surface.clear(20.0);
		double load = (double) surface.getLoad(2.0);
		assertEquals(0.0, load, 0.0);
	}
	
	


}