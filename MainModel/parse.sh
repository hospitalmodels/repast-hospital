#!/bin/bash

cat events.csv | grep ADMISSION > patients/admissions.csv
echo 'get admissions'
sed -n 's/.*Patient \([0-9]*\),/patients\/\1.csv/p' patients/admissions.csv > patients/patients.csv

echo 'get patient list'
cat patients/patients.csv
